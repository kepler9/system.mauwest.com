<?php
namespace app\controllers;
use app\models\Casuals;
use Yii;
use app\models\Labour;
use app\models\LabourSearch;
use app\controllers\MainController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
/**
 * LabourController implements the CRUD actions for Labour model.
 */
class LabourController extends MainController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST','GET'],
                ],
            ],
        ];
    }

    /**
     * Lists all Labour models.
     * @return mixed
     */
    public function actionIndex($type) {
        $searchModel = new LabourSearch();
        $searchModel->type = $type;
        $searchModel->project_id = $this->project->project_id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Labour model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Labour model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($type) {
        $model = new Labour();
        if(Yii::$app->request->post()){
            $model->load(Yii::$app->request->post());
            $model->quantity = 1;
            $query = Labour::find()->where(['type'=>$type]);
            if($type == 1){
                $model->title = $model->casual->name;
                $model->total = (double) $model->wht + (double) $model->overtime + (double) $model->transport + ($model->rate * $model->quantity);
                $query->andWhere(['casual_id'=>$model->casual_id,'date_created'=>$model->date_created]);
            }else if($type == 0){
                $model->rate = $model->total;
                $model->date_created = date('Y-m-d');
                $query->andWhere(['title'=>$model->title,'date_created'=>$model->date_created,]);
            }
            $model->project_id = $this->project->project_id;
            $model->type = $type;
            $exists = $query->one();
            if(!$exists){
                if ($model->save()) {
                    return $this->redirect(['index', 'type' => $model->type]);
                }
            }else{
                $error = $model->title. ' had already been recorded for date: '.date('M d, Y',strtotime($model->date_created));
                if($model->casual_id){
                    $model->addError('casual_id',$error);
                }else{
                    $model->addError('title',$error);
                }                
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Labour model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id,$redirect=null)
    {
        $model = $this->findModel($id);
        if(Yii::$app->request->post()){
            $model->load(Yii::$app->request->post());
            $model->quantity = 1;
            if($model->type == 0){
                $model->rate = $model->total;
            }else if($model->type ==1){
                $model->total = (double) $model->wht + (double) $model->overtime + (double) $model->transport + ($model->rate * $model->quantity);
            }
            if ($model->save()) {
                Yii::$app->session->setFlash('msg','Updated');
                if($redirect){
                    return $this->redirect([$redirect]);
                }
                return $this->redirect(['index', 'type' => $model->type]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Labour model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();

        return $this->redirect(['index','type'=>$model->type]);
    }

    /**
     * Finds the Labour model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Labour the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Labour::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionCasual($casual_id){
        return Json::encode(Casuals::find()->where(['casual_id'=>$casual_id])->asArray()->one());
    }

}

<?php
namespace app\controllers;
use app\models\Models;
/**
***
**/
class JobsController extends \yii\web\Controller
{
	public $model;

    public function beforeAction($action){
        $this->model = new Models();
        return parent::beforeAction($action);
    }

    public function actionMinute() {
        $this->model->send_emails();
        $this->model->send_sms();
    }

    public function actionDaily() {
        $this->model->project_status();
        $this->model->progress_tracking();
        $this->model->send_emails();
        $this->model->sms_batch();
        $this->model->send_sms();
    }

    public function actionWeekly(){
        $this->model->proposal_follow_up();
        $this->model->send_emails();
    }

    public function actionMailTemplate(){
        return $this->render('//../mail/template');
    }
}
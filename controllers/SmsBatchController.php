<?php

namespace app\controllers;
use AfricasTalking\SDK\AfricasTalking;
use Yii;
use app\models\SmsBatch;
use app\models\SmsBatchSearch;
use app\controllers\MainController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\CustomerLists;
use app\models\Customers;
use app\models\SmsQueue;
use app\models\SmsQueueSearch;
use app\models\SmsLogsSearch;
use yii\helpers\Json;

/**
 * SmsBatchController implements the CRUD actions for SmsBatch model.
 */
class SmsBatchController extends MainController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SmsBatch models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SmsBatchSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SmsBatch model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        $SmsQueueSearchModel = new SmsQueueSearch();
        $SmsQueueSearchModel->batch_id = $id;
        $SmsQueueDataProvider = $SmsQueueSearchModel->search([]);

        $SmsLogsSearchModel = new SmsLogsSearch();
        $SmsLogsSearchModel->batch_id = $id;
        $SmsLogsDataProvider = $SmsLogsSearchModel->search([]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'SmsQueueDataProvider' => $SmsQueueDataProvider,
            'SmsLogsDataProvider' => $SmsLogsDataProvider,
        ]);
    }

    /**
     * Creates a new SmsBatch model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($list_id=null)
    {
        $model = new SmsBatch();
        $model->list_id = $list_id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if(strtotime($model->send_date) <= time()){
                $this->model->sms_queue(['batch_id'=>$model->batch_id]);
            }
            return $this->redirect(['view', 'id' => $model->batch_id]);
        }

        return $this->render('create', [
            'model' => $model,
            'lists' => CustomerLists::find()->orderBy('list_id desc')->all(),
        ]);
    }

    

    /**
     * Updates an existing SmsBatch model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->batch_id]);
        }

        return $this->render('update', [
            'model' => $model,
            'lists' => CustomerLists::find()->orderBy('list_id desc')->all(),
        ]);
    }

    /**
     * Deletes an existing SmsBatch model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if(!$model->smsLogs){
            $model->delete();
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the SmsBatch model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SmsBatch the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SmsBatch::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    public function actionTopUp(){
        $settings = $this->model->system_settings();
        $AT = new AfricasTalking($settings->at_username, $settings->at_apikey);
        $application = $AT->application();
        $response = $application->fetchApplicationData();
        $results = Json::decode(Json::encode($response));
        $sms_balance = 0;
        if(isset($results['data']['UserData']['balance'])){
            $sms_balance = str_replace('KES ', '', $results['data']['UserData']['balance']);
        }
        return $this->render('top-up',['sms_balance'=>$sms_balance]);
    }
}

<?php
namespace app\controllers;
use Yii;
use app\models\Customers;
use app\models\CustomerLists;
use app\models\CustomersSearch;
use app\controllers\MainController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Projects;
use yii\web\UploadedFile;
use app\models\ExcelFileUpload;
use app\models\SmsQueue;
/**
 * CustomersController implements the CRUD actions for Customers model.
 */
class CustomersController extends MainController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Customers models.
     * @return mixed
     */
    public function actionIndex($list_id=null)
    {
        $searchModel = new CustomersSearch();
        $searchModel->list_id = $list_id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Customers model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id,$project_id=null)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'project' => $project_id ? Projects::findOne($project_id) : NULL,
        ]);
    }

    /**
     * Creates a new Customers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($redirect=null,$list_id=null)
    {
        $model = new Customers();
        if($list_id){
            $model->list_id = $list_id;
        }else{
            $list_0 = CustomerLists::find()->where(['type'=>'default'])->one();
            if($list_0){
                $model->list_id = $list_0->list_id;
            }
        }
        $ExcelFileUpload = new ExcelFileUpload();

        if(Yii::$app->request->post()){
            switch (Yii::$app->request->post('form')) {
                
                case 'create':
                    $model->load(Yii::$app->request->post());
                    if($model->phone){
                        $model->phone = $this->model->formatPhone($model->phone);
                    }
                    if ($model->validate() && $model->save()) {
                        if($redirect){
                            return $this->redirect([$redirect,'customer_id'=>$model->customer_id]);
                        }
                        return $this->redirect(['view', 'id' => $model->customer_id]);
                    }
                    break;
                
                case 'upload':
                    $ExcelFileUpload->load(Yii::$app->request->post());
                    $ExcelFileUpload->UploadFile = UploadedFile::getInstance($ExcelFileUpload, 'UploadFile');
                    if ($ExcelFileUpload->upload()) {
                       if($ExcelFileUpload->createCustomers(['list_id'=>Yii::$app->request->post('Customers')['list_id']])){
                            Yii::$app->session->setFlash('msg','Customers Uploaded');
                            if($redirect){
                                return $this->redirect([$redirect]);
                            }
                            return $this->redirect(['index']);
                       }
                    }
                    break;
            }
        }

        return $this->render('create', [
            'model' => $model,
            'lists' => CustomerLists::find()->orderBy('list_id desc')->all(),
            'ExcelFileUpload' => $ExcelFileUpload
        ]);
    }

    /**
     * Updates an existing Customers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
    */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if(Yii::$app->request->post()){
            $model->load(Yii::$app->request->post());
            if($model->phone){
                $model->phone = $this->model->formatPhone($model->phone);
            }
            if ($model->validate() && $model->save()) {
                return $this->redirect(['view', 'id' => $model->customer_id]);
            }
        }
        return $this->render('update', [
            'model' => $model,
            'lists' => CustomerLists::find()->all(),
        ]);
    }
    /**
     * Deletes an existing Customers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }
    
    /**
     * Finds the Customers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Customers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
    */
    
    protected function findModel($id)
    {
        if (($model = Customers::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionTemplate(){
        $path = '../web/templates/customers-template.xlsx';
        if(file_exists($path)){
            return Yii::$app->response->sendFile($path);
        }else{
            throw new NotFoundHttpException('The requested template does not exist.');
        }
    }

    public function actionMessage($customer_id,$redirect=null){
        $queue = new SmsQueue();
        $queue->customer_id = $customer_id;
        $customer = Customers::findOne($customer_id);
        if(Yii::$app->request->post()){
            $queue->phone = mb_substr($customer->phone, -9);
            $queue->load(Yii::$app->request->post());
            if($queue->save()){
                Yii::$app->session->setFlash('msg','SMS Queued for Sending');
                if($redirect){
                    return $this->redirect($redirect);
                }else{
                    return $this->redirect(['view','id'=>$customer_id]);
                }
            }
        }
        return $this->render('message',['customer'=>$customer,'queue'=>$queue]);
    }
}

<?php
namespace app\controllers;
use yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\Models;
use app\models\Projects;
/**
 * main controller
 */
class MainController extends \yii\web\Controller
{
	public $model;
    public $project;

	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

	public function beforeAction($action){
		$this->layout = 'admin';
		$this->model = new Models();
        $this->project = Yii::$app->session->get('project');
		return parent::beforeAction($action);
	}
    public function error_msg($msg="Failed") {
        Yii::$app->session->setFlash('error_msg',true);
        if($msg){
            Yii::$app->session->setFlash('msg',$msg);
        }
    }
    public function msg($msg = 'Success'){
        Yii::$app->session->setFlash('msg',$msg);
    }
}
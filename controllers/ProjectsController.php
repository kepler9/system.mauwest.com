<?php

namespace app\controllers;

use Yii;
use app\models\Projects;
use app\models\Customers;
use app\models\CustomerLists;
use app\models\Materials;
use app\models\Labour;
use app\models\ProjectsSearch;
use app\controllers\MainController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\DocumentTypes;
use app\models\Documents;
use app\models\DocumentsSearch;
use app\models\Sites;

/**
 * ProjectsController implements the CRUD actions for Projects model.
 */
class ProjectsController extends MainController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Projects models.
     * @return mixed
     */
    public function actionIndex($status_id=null,$customer_id=null)
    {
        $model = new Projects();
        $model->status_id = $status_id;
        $model->customer_id = $customer_id;
        $searchModel = new ProjectsSearch();
        $searchModel->status_id = $model->status_id;
        $searchModel->customer_id = $model->customer_id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Projects model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        Yii::$app->session->set('project',$model);
        $documentsSearchModel = new DocumentsSearch();
        $documentsSearchModel->project_id = $id;
        $documentsDataProvider = $documentsSearchModel->search([]);
        return $this->render('view', [
            'model' => $model,
            'documentsDataProvider' => $documentsDataProvider
        ]);
    }

    /**
     * Creates a new Projects model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($step=1,$site_id=null) {
        $this->project = Yii::$app->session->has('project_id') ? Projects::findOne(Yii::$app->session->get('project_id')) : new Projects();
        switch ($step) {
            
            case 1:
                $model = $this->project;
                $form = "_form";
                $title = "New Project";
                if(Yii::$app->request->post()){
                    $model->load(Yii::$app->request->post());
                    $model->name = $model->description;
                    $model->user_id = Yii::$app->user->identity->id;
                    if ($model->save()) {
                        if($site_id){
                            $site = Sites::findOne($site_id);
                            $site->project_id = $model->project_id;
                            $site->save();
                        }
                        Yii::$app->session->set('project_id',$model->project_id);
                        $step = 2;
                        return $this->redirect(['create', 'step' => $step]);
                    }
                }
                if($site_id){
                    $site = Sites::findOne($site_id);
                    $model->customer_id = $site->customer_id;
                    $model->description = $site->description;
                    $model->location = $site->location;
                    $model->estimated_area = $site->area;
                    $model->status_id = 1;
                }
                break;

            case 2:
                $model = new Customers();
                $form = "_documents";
                $title = "Upload Documents";
                $project = $this->project;
                if(Yii::$app->request->post()){
                    $documentTypes = DocumentTypes::find()->all();
                    if($documentTypes){
                        foreach($documentTypes as $type){
                            $document = new Documents();
                            $document->project_id = $project->project_id;
                            $file = $this->model->upload_file(['field'=>'file_'.$type->document_type_id]);
                            if($file){
                                $document->file = $file;
                            }
                            $document->document_type_id = $type->document_type_id;
                            $document->save();
                        }
                        return $this->redirect(['create', 'step' => 3]);
                    }
                }
                break;

            case 3:
                $model = new Materials();
                $title = "Estimated Materials";
                $form = "_materials";
                $project = $this->project;
                if(Yii::$app->request->post()){
                    $data = Yii::$app->request->post();
                    $next = false;
                    if($data['item']){
                        for ($i=0; $i < count($data['item']); $i++) { 
                            if($data['item'][$i]){
                                $model = new Materials();
                                $model->item = $data['item'][$i];
                                $model->quantity = $data['quantity'][$i];
                                $model->unit = $data['unit'][$i];
                                $model->rate = $data['rate'][$i];
                                $model->total = $model->quantity * $model->rate;
                                $model->project_id = $project->project_id;
                                if($model->save()){
                                    $next = true;
                                }
                            }
                        }
                    }
                    if($next){
                        return $this->redirect(['create', 'step' => 4]);
                    }
                }
                break;

            case 4:
                $model = new Labour();
                $title = "Estimated Labour";
                $form = "_labour";
                $project = $this->project;
                if(Yii::$app->request->post()){
                    $data = Yii::$app->request->post();
                    $next = false;
                    if($data['title']){
                        for ($i=0; $i < count($data['title']); $i++) { 
                            if($data['title'][$i]){
                                $model = new Labour();
                                $model->title = $data['title'][$i];
                                $model->total = $data['total'][$i];
                                $model->rate = $data['total'][$i];
                                $model->quantity = 1;
                                $model->project_id = $project->project_id;
                                if($model->save()){
                                    $next = true;
                                }
                            }
                        }
                    }
                    if($next){
                        $this->model->project_created($project->project_id);
                        Yii::$app->session->remove('project_id');
                        return $this->redirect(['view', 'id' => $project->project_id]);
                    }
                }
                break;
            
            default:
                throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
                break;
        }

        return $this->render('create', [
            'model' => $model,
            'form' => $form,
            'title' => $title,
            'step' => $step,
            'lists' => CustomerLists::find()->where(['type'=>'default'])->all(),
        ]);
    }

    /**
     * Updates an existing Projects model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id,$type=null) {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $model->name = $model->description;
            if($model->save()){
                if($model->status_id == 4){
                    $this->model->project_completed($model->project_id);
                }
                if($model->status_id == 7){
                    $this->model->project_lost($model->project_id);
                }
                Yii::$app->session->remove('project_id');
                return $this->redirect(['view', 'id' => $model->project_id]);
            }
        }
        return $this->render('update', [
            'model' => $model,
            'type' => $type,
        ]);
    }
    /**
     * Deletes an existing Projects model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->remove('project_id');
        return $this->redirect(['index']);
    }

    /**
     * Finds the Projects model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Projects the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Projects::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    public function actionUpdateDocuments($project_id){
        $project = $this->findModel($project_id);
        if(Yii::$app->request->post()){
            $documentTypes = DocumentTypes::find()->all();
            if($documentTypes){
                foreach($documentTypes as $type){
                    $file = $this->model->upload_file(['field'=>'file_'.$type->document_type_id]);
                    if($file){
                        $document = Documents::findOne(['project_id'=>$project->project_id,'document_type_id'=>$type->document_type_id]);
                        if(!$document){
                            $document = new Documents();
                            $document->document_type_id = $type->document_type_id;
                            $document->project_id = $project->project_id;
                        }
                        $document->file = $file;
                        if($document->save()){
                            Yii::$app->session->setFlash('Documents Updated');
                        }
                    }
                }
                return $this->redirect(['view', 'id' => $project_id]);
            }
        }
        return $this->render('update-documents',[
            'project' => $project
        ]);
    }
    public function actionDownloadDocument($id){
        $model = Documents::findOne($id);
        if($model){
            if(file_exists('uploads/'.$model->file)){
                return Yii::$app->response->sendFile('uploads/'.$model->file);
            }else{
                throw new NotFoundHttpException('The requested file does not exist.');
            }
        }
    }
    public function actionDeleteDocument($id){
        $model = Documents::findOne($id);
        if($model){
            if(file_exists('uploads/'.$model->file)){
                unlink('uploads/'.$model->file);
            }
            $model->delete();
            Yii::$app->session->setFlash('msg','Document deleted');
        }
        return $this->redirect(Yii::$app->request->referrer);
    }
    public function actionEditDocument($id){
        $model = Documents::findOne($id);
        if($model){
            if(Yii::$app->request->post()){
                $model->load(Yii::$app->request->post());
                if($model->save()){
                    return $this->redirect(['view','id'=>$model->project_id]);
                }
            }else{
                return $this->render('_form-document-details',['model'=>$model]);
            }
        }
        return $this->redirect(Yii::$app->request->referrer);
    }
}

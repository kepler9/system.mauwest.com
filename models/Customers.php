<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customers".
 *
 * @property int $customer_id
 * @property string $name
 * @property string|null $phone
 * @property string|null $email
 * @property string|null $company
 * @property int $status
 * @property int $list_id
 * @property string $date_created
 *
 * @property CustomerLists $list
 * @property Projects[] $projects
 * @property Sites[] $sites
 * @property SmsLogs[] $smsLogs
 * @property SmsQueue[] $smsQueues
 */
class Customers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'list_id'], 'required'],
            [['status', 'list_id'], 'integer'],
            [['date_created'], 'safe'],
            [['name', 'email'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 20],
            [['company'], 'string', 'max' => 500],
            [['phone'], 'unique'],
            [['list_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustomerLists::className(), 'targetAttribute' => ['list_id' => 'list_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'customer_id' => 'Customer ID',
            'name' => 'Name',
            'phone' => 'Phone',
            'email' => 'Email',
            'company' => 'Company',
            'status' => 'Status',
            'list_id' => 'List ID',
            'date_created' => 'Date Created',
        ];
    }

    /**
     * Gets query for [[List]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getList()
    {
        return $this->hasOne(CustomerLists::className(), ['list_id' => 'list_id']);
    }

    /**
     * Gets query for [[Projects]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Projects::className(), ['customer_id' => 'customer_id']);
    }

    /**
     * Gets query for [[Sites]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSites()
    {
        return $this->hasMany(Sites::className(), ['customer_id' => 'customer_id']);
    }

    /**
     * Gets query for [[SmsLogs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSmsLogs()
    {
        return $this->hasMany(SmsLogs::className(), ['customer_id' => 'customer_id']);
    }

    /**
     * Gets query for [[SmsQueues]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSmsQueues()
    {
        return $this->hasMany(SmsQueue::className(), ['customer_id' => 'customer_id']);
    }
}

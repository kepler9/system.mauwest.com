<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "project_status".
 *
 * @property int $status_id
 * @property string $name
 * @property string $alert
 * @property string $description
 *
 * @property Projects[] $projects
 */
class ProjectStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'project_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'alert', 'description'], 'required'],
            [['description'], 'string'],
            [['name', 'alert'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'status_id' => 'Status ID',
            'name' => 'Name',
            'alert' => 'Alert',
            'description' => 'Description',
        ];
    }

    /**
     * Gets query for [[Projects]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Projects::className(), ['status_id' => 'status_id']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $email
 * @property string|null $password_hash
 * @property string|null $authKey
 * @property string|null $access_token
 * @property int $status
 * @property string|null $date_created
 * @property string $phone_no
 * @property int $role
 * @property int $notifications
 *
 * @property Projects[] $projects
 */
class Admins extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'email', 'phone_no'], 'required'],
            [['status', 'role', 'notifications'], 'integer'],
            [['date_created'], 'safe'],
            [['username', 'phone_no'], 'string', 'max' => 50],
            [['email'], 'string', 'max' => 100],
            [['password_hash', 'authKey', 'access_token'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'email' => 'Email',
            'password_hash' => 'Password Hash',
            'authKey' => 'Auth Key',
            'access_token' => 'Access Token',
            'status' => 'Status',
            'date_created' => 'Date Created',
            'phone_no' => 'Phone No',
            'role' => 'Role',
            'notifications' => 'Notifications',
        ];
    }

    /**
     * Gets query for [[Projects]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Projects::className(), ['user_id' => 'id']);
    }
    public function getAccountStatus(){
        switch ($this->status) {
            case '1':
                return '<span class="badge badge-success">Active</span>';
                break;
            
            default:
                return '<span class="badge badge-danger">Inactive</span>';
                break;
        }
    }
    public function getNotificationsStatus(){
        switch ($this->notifications) {
            case 1:
                return '<span class="badge badge-primary">On</span>';
                break;
            
            default:
                return '<span class="badge badge-danger">Off</span>';
                break;
        }
    }
    public function format_phone(){
        return substr(str_replace(' ', '', $this->phone_no), -9);
    }
    public function format_username(){
        return str_replace(' ', '', ucwords(strtolower($this->username)));
    }
}

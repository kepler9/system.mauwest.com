<?php
namespace app\models;
use yii\data\ActiveDataProvider;
use yii;
use AfricasTalking\SDK\AfricasTalking;
use yii\helpers\Json;
/**
 * 
 */
class Models extends \yii\base\Model
{
	public function system_settings(){
        $system_settings = SystemSettings::find()->asArray()->all();
        foreach($system_settings as $key){
            $settings[$key['settings']] = $key['value'];
        }
        return (object) $settings;
    }

    public function queue_email($params){
        $settings = $this->system_settings();
        $queue = new MailQueue();
        $queue->email_to = $params['to'];
        $queue->subject = $params['subject'];
        $queue->html_body = $params['body'];
        if(isset($params['bcc'])){
            if(is_bool($params['bcc'])){
            	if($params['bcc'] == true){
            		$queue->bcc = $settings->admin_email;
            	}
            }else{
                $queue->bcc = $params['bcc'];
            }
        }
        if(isset($params['cc'])){
            $queue->cc = $params['cc'];
        }
        if(isset($params['attachment'])){
            $queue->attachments = $params['attachment'];
        }
        return $queue->save() ? true : false;
    }
    
    public function send_emails() {
		$system_settings = $this->system_settings();
		if($system_settings->email_enabled){
			$queue = MailQueue::find()->where(['sent' => 0])->limit(30)->all();
		  	if($queue){
		  		foreach($queue as $key){
		  			$key->sent = 1;
		  			if($key->save()){
		  				$mailer = Yii::$app->mailer->compose('template' , ['body'=>$key->html_body,'subject'=>$key->subject]);
				        $mailer->setFrom([$system_settings->sender_email => $system_settings->sender_name]);
				        $mailer->setTo($key->email_to);
				        if(isset($key->cc)){
				        	$mailer->setCc($key->cc);
				        }
				        if(isset($key->bcc)){
				        	$mailer->setBcc($key->bcc);
				        }
				        if(isset($key->subject)){
				        	$mailer->setSubject($key->subject);
				        }
						if(isset($key->attachments)){
							$mailer->attach('../web/uploads/'.$key->attachments);
						}
						$mailer->setReplyTo($system_settings->reply_to);
						if($mailer->send()){
							$key->delete();
							return true;
						}
		  			}
		  		}
		  	}
		}
	}
    public function send_sms(){
        $settings = $this->system_settings();
        if($settings->sms_enabled){
            $SmsQueue = SmsQueue::find()->where(['status'=>'queued'])->all();
            if($SmsQueue){
                $AT = new AfricasTalking($settings->at_username, $settings->at_apikey);
                $sms = $AT->sms();
                foreach($SmsQueue as $key){
                    $key->status = 'sending';
                    if($key->save()){
                        $response = $sms->send([
                            'to' => '+254'.$key->phone,
                            'message' => $key->message."\n\nSTOP *456*9*5#",
                            'from' => $settings->sender_id,
                        ]);
                        $result = Json::decode(Json::encode($response));
                        if(isset($result['data']['SMSMessageData']['Recipients'])){
                            $result = array_pop($result['data']['SMSMessageData']['Recipients']);
                            if($result){
                                $SmsLogs = new SmsLogs();
                                $SmsLogs->batch_id = $key->batch_id;
                                $SmsLogs->customer_id = $key->customer_id;
                                $SmsLogs->phone = $result['number'];
                                $SmsLogs->message = $key->message;
                                $SmsLogs->status = $result['status'];
                                $SmsLogs->cost = str_replace('KES ', '', $result['cost']);
                                if($SmsLogs->save()){
                                    $key->delete();
                                }
                            }
                        }
                    }
                }
            }
        }
    }
	public function days_between_dates($start_date,$end_date){
		$earlier = new \DateTime($start_date);
        $later = new \DateTime($end_date);
        return $later->diff($earlier)->format("%a");
	}
    public function project_duration($project_id){
    	$model = Projects::findOne($project_id);
        $days = $this->days_between_dates($model->start_date,$model->end_date);   
        $mod = $days % 7;
        $weeks = floor($days/7);
        if($mod){
            $w_text = $weeks > 1 ? "weeks" : "week";
            $d_text = $days > 1 ? "days" : "day";
            $m_text = $mod > 1 ? "days" : "day";
            return $days >= 7 ? $weeks." ".$w_text." & ".$mod." ".$m_text : $days." ".$d_text;
        }else{
            $w_text = $weeks > 1 ? "weeks" : "week";
            return $days >= 7 ? $weeks." ".$w_text : $days." days";
        }    
    }

    public function actual_project_duration($project_id){
        $days = Labour::find()->where(['project_id'=>$project_id,'type'=>1])->select('date_created')->distinct()->count();
        $mod = $days % 7;
        $weeks = floor($days/7);
        if($mod){
            $w_text = $weeks > 1 ? "weeks" : "week";
            $d_text = $days > 1 ? "days" : "day";
            $m_text = $mod > 1 ? "days" : "day";
            return $days >= 7 ? $weeks." ".$w_text." & ".$mod." ".$m_text : $days." ".$d_text;
        }else{
            $w_text = $weeks > 1 ? "weeks" : "week";
            return $days >= 7 ? $weeks." ".$w_text : $days." days";
        }
    }

    public function estimated_costs($project_id){
    	$estimated_materials_cost = $this->estimated_materials_cost($project_id);
    	$estimated_labour_cost = $this->estimated_labour_cost($project_id);
    	return $estimated_materials_cost + $estimated_labour_cost;
    }

    public function estimated_materials_cost($project_id){
    	return Materials::find()->where(['project_id'=>$project_id,'type'=>0])->sum('total') ?? 0;
    }

    public function estimated_labour_cost($project_id){
    	return Labour::find()->where(['project_id'=>$project_id,'type'=>0])->sum('total') ?? 0;
    }

    public function actual_costs($project_id){
    	$actual_materials_cost = $this->actual_materials_cost($project_id);
    	$actual_labour_cost = $this->actual_labour_cost($project_id);
        $misc_costs = $this->misc_costs($project_id);
    	return $actual_materials_cost + $actual_labour_cost + $misc_costs;
    }
    public function actual_materials_cost($project_id){
    	return Materials::find()->where(['project_id'=>$project_id,'type'=>1])->sum('total') ?? 0;
    }
    public function actual_labour_cost($project_id){
    	return Labour::find()->where(['project_id'=>$project_id,'type'=>1])->sum('total') ?? 0;
    }
    public function misc_costs($project_id){
        return MiscCosts::find()->where(['project_id'=>$project_id])->sum('cost');
    }
    public function miscellaneous_costs($project_id,$return_type){
        $query = MiscCosts::find()->where(['project_id'=>$project_id]);
        $misc_costs = $query->all();
        if($return_type == 'grid'){
            $search_model = new MiscCostsSearch();
            $search_model->project_id = $project_id;
            return $search_model->search(Yii::$app->request->queryParams);
        }
    }
    public function estimated_materials($project_id,$return_type = null){
        $query = Materials::find()->where(['project_id'=>$project_id,'type'=>0]);
        $materials = $query->all();
        if($return_type == null){
            return $materials;
        }
        if($return_type == 'string'){
            if($materials){
                $string = "";
                foreach($materials as $key){
                    $string .= $key->item.", ";
                }
                return rtrim($string,', ');
            }
        }
        if($return_type == 'grid'){
            $search_model = new MaterialsSearch();
            $search_model->project_id = $project_id;
            $search_model->type = 0;
            return $search_model->search(Yii::$app->request->queryParams);
        }
    }

    public function actual_materials($project_id,$return_type = null){
        $query = Materials::find()->where(['project_id'=>$project_id,'type'=>1]);
        $materials = $query->all();
        if($return_type == null){
            return $materials;
        }
        if($return_type == 'string'){
            if($materials){
                $string = "";
                foreach($materials as $key){
                    $string .= $key->item.", ";
                }
                return rtrim($string,', ');
            }
        }
        if($return_type = 'grid'){
            $search_model = new MaterialsSearch();
            $search_model->project_id = $project_id;
            $search_model->type = 1;
            return $search_model->search(Yii::$app->request->queryParams);
        }
    }

    public function estimated_labour($project_id,$return_type = null){
    	$query = Labour::find()->where(['project_id'=>$project_id,'type'=>0]);
        $labour = $query->all();
        if($return_type == null){
            return $labour;
        }
        if($return_type == 'string'){
            if($labour){
                $string = "";
                foreach($labour as $key){
                    $string .= $key->title.", ";
                }
                return rtrim($string,', ');
            }
        }
        if($return_type == 'grid'){
            $search_model = new LabourSearch();
            $search_model->project_id = $project_id;
            $search_model->type = 0;
            return $search_model->search(Yii::$app->request->queryParams);
        }
    }

    public function actual_labour($project_id,$return_type = null){
        $query = Labour::find()->where(['project_id'=>$project_id,'type'=>1]);
        $labour = $query->all();
        if($return_type == null){
            return $labour;
        }
        if($return_type == 'string'){
            if($labour){
                $string = "";
                foreach($labour as $key){
                    $string .= $key->title.", ";
                }
                return rtrim($string,', ');
            }
        }
        if($return_type == 'grid'){
            $search_model = new LabourSearch();
            $search_model->project_id = $project_id;
            $search_model->type = 1;
            return $search_model->search(Yii::$app->request->queryParams);
        }
    }

    public function notify($params){
        $admins = Admins::find()->where(['notifications'=>1,'status'=>1])->all();
        $settings = $this->system_settings();
        if($admins){
            foreach($admins as $key){
                $params['body'] = str_replace('[ADMIN]', $key->username, $params['body']);
                $this->queue_email([
                    'to' => $key->email,
                    'subject' => $params['subject'],
                    'body' => $params['body'],
                    'attachment' => isset($params['attachment']) ? $params['attachment'] : NULL,
                    'bcc' => false,
                ]);
            }
        }
    }
    public function project_created($project_id){
    	$template = EmailTemplates::findOne(['type'=>'project_created']);
    	if($template){
    		$project = Projects::findOne($project_id);
    		$template->body = str_replace('[PROJECT]', ucwords(strtolower($project->name)), $template->body);
    		$template->body = str_replace('[AMOUNT]', number_format($project->amount_charged), $template->body);
    		$template->body = str_replace('[USER]', $project->user->username, $template->body);
    		$template->body = str_replace('[CUSTOMER]', ucwords(strtolower($project->customer->name)), $template->body);
            $template->body = str_replace('[AREA]', $project->estimated_area, $template->body);
    		$template->body = str_replace('[STARTDATE]', date('d/m/Y',strtotime($project->start_date)), $template->body);
    		$template->body = str_replace('[ENDDATE]', date('d/m/Y',strtotime($project->end_date)), $template->body);
    		$template->body = str_replace('[DURATION]', $this->project_duration($project->project_id), $template->body);
    		$template->body = str_replace('[MATERIALSCOST]', number_format($this->estimated_materials_cost($project->project_id)), $template->body);
            $template->body = str_replace('[ESTIMATEDMATERIALS]', $this->estimated_materials($project->project_id,'string'), $template->body);
    		$template->body = str_replace('[ESTIMATEDLABOUR]', $this->estimated_labour($project->project_id,'string'), $template->body);
            $template->body = str_replace('[LABOURCOST]', number_format($this->estimated_labour_cost($project->project_id)), $template->body);
    		$template->subject = str_replace(['[PROJECT]'], ucwords(strtolower($project->name)), $template->subject);
            $this->notify([
                'body'=>$template->body,
                'subject'=>$template->subject
            ]);
    	}
    }
    public function progress_tracking(){
        $settings = $this->system_settings();
        $projects = Projects::find()->where(['status_id'=>[2,6]])->all();
        if($projects){
        	foreach($projects as $project){
        		$template = EmailTemplates::findOne(['type'=>'progress_tracking']);
        		if($template){
        			$template->body = str_replace('[PROJECT]', ucwords(strtolower($project->name)), $template->body);
                    $template->body = str_replace('[STARTDATE]', date('d/m/Y',strtotime($project->start_date)), $template->body);
                    $template->body = str_replace('[ENDDATE]', date('d/m/Y',strtotime($project->end_date)), $template->body);
                    $template->body = str_replace('[STATUS]', ucwords(strtolower($project->status->name)), $template->body);
					$template->body = str_replace('[LINK]', $settings->host.'/projects/view?id='.$project->project_id, $template->body);
					$template->subject = str_replace(['[PROJECT]'], ucwords(strtolower($project->name)), $template->subject);
                    $this->notify([
                        'body' => $template->body,
                        'subject' => $template->subject,
                    ]);
        		}
        	}
        }
    }
    public function project_status(){
    	$projects = Projects::find()->where(['status_id'=>[1,2]])->all();
    	if($projects){
    		foreach($projects as $project){
    			if($project->status_id == 2){
    				$due_days = $this->days_between_dates(date('Y-m-d'),$project->end_date);
	    			if($due_days == 3){
	    				$template = EmailTemplates::findOne(['type'=>'project_due']);
	    				$template->body = str_replace('[PROJECT]', ucwords(strtolower($project->name)), $template->body);
                        $template->body = str_replace('[CUSTOMER]', ucwords(strtolower($project->customer->name)), $template->body);
                        $template->body = str_replace('[AREA]', $project->estimated_area, $template->body);
						$template->body = str_replace('[ENDDATE]', date('d/m/Y',strtotime($project->end_date)), $template->body);
						$template->subject = str_replace(['[PROJECT]'], ucwords(strtolower($project->name)), $template->subject);
                        $this->notify([
                            'body'=>$template->body,
                            'subject'=>$template->subject
                        ]);
	    			}
	    			$template = EmailTemplates::findOne(['type'=>'project_costs']);
	    			if($template){
	    				$estimated_materials_cost = $this->estimated_materials_cost($project->project_id);
    					$actual_materials_cost = $this->actual_materials_cost($project->project_id);
    					if($actual_materials_cost > $estimated_materials_cost && $actual_materials_cost > $project->actual_materials_cost){
    						$body = str_replace('[PROJECT]',ucwords(strtolower($project->name)), $template->body);
                            $body = str_replace('[CUSTOMER]', ucwords(strtolower($project->customer->name)), $body);
                            $body = str_replace('[AREA]', $project->estimated_area, $body);
							$body = str_replace('[COSTITEM]','Materials', $body);
							$body = str_replace('[ESTIMATEDAMOUNT]',number_format($estimated_materials_cost), $body);
							$body = str_replace('[ACTUALAMOUNT]',number_format($actual_materials_cost), $body);
                            $template->subject = str_replace(['[PROJECT]'], ucwords(strtolower($project->name)), $template->subject);
							$this->notify([
                                'body'=>$body,
                                'subject'=>$template->subject
                            ]);
    					}
                        $project->actual_materials_cost = $actual_materials_cost;

    					$estimated_labour_cost = $this->estimated_labour_cost($project->project_id);
    					$actual_labour_cost = $this->actual_labour_cost($project->project_id);

    					if($actual_labour_cost > $estimated_labour_cost && $actual_labour_cost > $project->actual_labour_cost){
    						$body = str_replace('[PROJECT]',ucwords(strtolower($project->name)), $template->body);
                            $body = str_replace('[CUSTOMER]', ucwords(strtolower($project->customer->name)), $body);
                            $body = str_replace('[AREA]', $project->estimated_area, $body);
							$body = str_replace('[COSTITEM]','Labour', $body);
							$body = str_replace('[ESTIMATEDAMOUNT]',number_format($estimated_labour_cost), $body);
							$body = str_replace('[ACTUALAMOUNT]',number_format($actual_labour_cost), $body);
                            $template->subject = str_replace(['[PROJECT]'], ucwords(strtolower($project->name)), $template->subject);
							$this->notify([
                                'body'=>$body,
                                'subject'=>$template->subject
                            ]);
    					}
                        $project->actual_labour_cost = $actual_labour_cost;
	    			}
    			}
    			$template = NULL;
    			if($project->start_date == date('Y-m-d')){
    				$project->status_id = 2;
    				$template = EmailTemplates::findOne(['type'=>'project_started']);
    			}
    			if($project->end_date <= date('Y-m-d')){
    				$project->status_id = 6;
                    if($project->end_date == date('Y-m-d')){
                        $template = EmailTemplates::findOne(['type'=>'project_ended']);
                    }
    			}
    			if($project->save()){
                    if($template){
                        $template->body = str_replace('[PROJECT]', ucwords(strtolower($project->name)), $template->body);
                        $template->body = str_replace('[CUSTOMER]', ucwords(strtolower($project->customer->name)), $template->body);
                        $template->body = str_replace('[AREA]', $project->estimated_area, $template->body);
                        $template->body = str_replace('[STARTDATE]', date('d/m/Y',strtotime($project->start_date)), $template->body);
                        $template->body = str_replace('[ENDDATE]', date('d/m/Y',strtotime($project->end_date)), $template->body);
                        $template->subject = str_replace(['[PROJECT]'], ucwords(strtolower($project->name)), $template->subject);
                        $this->notify([
                            'body'=>$template->body,
                            'subject'=>$template->subject
                        ]);
                    }
    			}
    		}
    	}
    }
    public function project_completed($project_id){
        $project = Projects::findOne($project_id);
        if($project->status_id == 4){
            $template = EmailTemplates::findOne(['type'=>'project_completed']);
            if($template){
                $template->body = str_replace('[PROJECT]', ucwords(strtolower($project->name)), $template->body);
                $template->body = str_replace('[LOCATION]', $project->location, $template->body);
                $template->body = str_replace('[AMOUNT]', number_format($project->amount_charged), $template->body);
                $template->body = str_replace('[CUSTOMER]', ucwords(strtolower($project->customer->name)), $template->body);
                $template->body = str_replace('[AREA]', $project->estimated_area, $template->body);
                $template->body = str_replace('[NOTES]', $project->notes, $template->body);
                $template->body = str_replace('[STARTDATE]', date('d/m/Y',strtotime($project->start_date)), $template->body);
                $template->body = str_replace('[ENDDATE]', date('d/m/Y',strtotime($project->end_date)), $template->body);
                $template->body = str_replace('[COMPLETEDATE]', date('d/m/Y'), $template->body);
                $template->body = str_replace('[ESTIMATEDMATERIALS]', $this->estimated_materials($project->project_id,'string'), $template->body);
                $template->body = str_replace('[ACTUALMATERIALS]', $this->actual_materials($project->project_id,'string'), $template->body);
                $template->body = str_replace('[ESTIMATEDCOST]', number_format($this->estimated_costs($project->project_id)), $template->body);
                $template->body = str_replace('[ACTUALCOST]', number_format($this->actual_costs($project->project_id)), $template->body);
                $template->body = str_replace('[PROFIT]', number_format($project->amount_charged - $this->actual_costs($project->project_id)), $template->body);
                $template->subject = str_replace(['[PROJECT]'], ucwords(strtolower($project->name)), $template->subject);
                $this->notify([
                    'body'=>$template->body,
                    'subject'=>$template->subject
                ]);
            }
        }
    }
    public function project_lost($project_id){
        $project = Projects::findOne($project_id);
        if($project->status_id == 7){
            $template = EmailTemplates::findOne(['type'=>'project_lost']);
            if($template){
                $template->body = str_replace('[PROJECT]', ucwords(strtolower($project->name)), $template->body);
                $template->body = str_replace('[AMOUNT]', number_format($project->amount_charged), $template->body);
                $template->body = str_replace('[CUSTOMER]', ucwords(strtolower($project->customer->name)), $template->body);
                $template->body = str_replace('[STATUS]', $project->status->name, $template->body);
                $template->body = str_replace('[NOTES]', $project->notes, $template->body);
                $template->subject = str_replace(['[PROJECT]'], ucwords(strtolower($project->name)), $template->subject);
                $this->notify([
                    'body'=>$template->body,
                    'subject'=>$template->subject
                ]);
            }
        }
    }
    public function proposal_follow_up(){
        $document_type = DocumentTypes::findOne(['emailed'=>1]);
        if($document_type){
            $documents = Documents::find()->where(['document_type_id'=>$document_type->document_type_id])->all();
            if($documents){
                foreach($documents as $document){
                    if($document->file){
                        if(in_array($document->project->status_id, [1])){
                            $project = $document->project;
                            $template = EmailTemplates::findOne(['type'=>'proposal_follow_up']);
                            if($template){
                                $template->body = str_replace('[PROJECT]', ucwords(strtolower($project->name)), $template->body);
                                $template->body = str_replace('[AMOUNT]', number_format($project->amount_charged), $template->body);
                                $template->body = str_replace('[CUSTOMER]', ucwords(strtolower($project->customer->name)), $template->body);
                                $template->subject = str_replace(['[PROJECT]'], ucwords(strtolower($project->name)), $template->subject);
                                $this->notify([
                                    'body'=>$template->body,
                                    'subject'=>$template->subject,
                                    'attachment'=>$document->file
                                ]);
                            }
                        }
                    }
                }
            }
        }
    }
    public function formatPhone($phone){
        $phone = str_replace(' ', '', $phone);
        return preg_replace('/[^A-Za-z0-9\-]/', '', $phone);
    }
    public function upload_file($data = null,array $options = null){
        if(isset($_FILES)){
            if(isset($_FILES[$data['field']]['name'])){
                $target_dir = (isset($data['path'])) ? $data['path'] : "uploads/";
                # rename file
                $file_name = rand(100,1000).'-'.str_replace(' ', '-', basename($_FILES[$data['field']]["name"]));
                if(move_uploaded_file($_FILES[$data['field']]['tmp_name'], $target_dir.$file_name)) {
                    return $file_name;
                }
            }
        }
    }
    public function upload_files($data,array $options = null){
        if(isset($_FILES)){
            if(isset($_FILES[$data['field']]['name'])){
                $target_dir = (isset($data['path'])) ? $data['path'] : "uploads/";
                $count = count($_FILES[$data['field']]['name']);
                if($count){
                    $files = array();
                    for($i = 0; $i < $count; $i++){
                        // rename file
                        $file_name = rand(100,1000).'-'.str_replace(' ', '-', basename($_FILES[$data['field']]["name"][$i]));
                        if(move_uploaded_file($_FILES[$data['field']]['tmp_name'][$i], $target_dir.$file_name)) {
                            $files[] = $file_name;
                        }
                    }
                    return $files;
                }
            }
        }
    }
    public function sms_batch(){
        $models = SmsBatch::find()->where(['processed'=>0])->andWhere(['<=','send_date',date('Y-m-d H:i:s')])->all();
        if($models){
            foreach($models as $model){
                $this->sms_queue(['batch_id'=>$model->batch_id]);
            }
        }
    }
    public function sms_queue($params){
        $model = SmsBatch::findOne(['batch_id'=>$params['batch_id'],'processed'=>0]);
        if($model){
            $customers = Customers::find()->where(['list_id'=>$model->list_id])->all();
            if($customers){
                foreach($customers as $customer){
                    if($customer->phone){
                        $phone = mb_substr($customer->phone, -9);
                        if(strlen($phone) == 9){
                            $queue = SmsQueue::find()->where(['customer_id'=>$customer->customer_id,'batch_id'=>$model->batch_id])->orWhere(['phone'=>mb_substr($customer->phone, -9),'batch_id'=>$model->batch_id])->one();
                            if(!$queue){
                                $queue = new SmsQueue();
                                $queue->batch_id = $model->batch_id;
                                $queue->customer_id = $customer->customer_id;
                            }
                            $queue->phone = $phone;
                            $queue->message = str_replace(['[NAME]'], ucwords(strtolower($customer->name)), $model->message);
                            $queue->save();
                        }
                    }
                }
                $model->processed = 1;
                $model->save();
            }
        }
    }
    public function projects_pie_report(){
        $data = [];
        $statuses = ProjectStatus::find()->all();
        if($statuses){
            foreach($statuses as $status){
                $data[] = array($status->name, (int) Projects::find()->where(['status_id'=>$status->status_id])->count());
            }
        }
        return $data;
    }
}
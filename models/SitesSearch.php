<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Sites;

/**
 * SitesSearch represents the model behind the search form of `app\models\Sites`.
 */
class SitesSearch extends Sites
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['site_id', 'customer_id', 'project_id', 'sketch', 'user_id'], 'integer'],
            [['date_visited', 'location', 'distance', 'description', 'area', 'photos', 'notes', 'date_created'], 'safe'],
            [['fare'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Sites::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'site_id' => $this->site_id,
            'customer_id' => $this->customer_id,
            'project_id' => $this->project_id,
            'date_visited' => $this->date_visited,
            'fare' => $this->fare,
            'sketch' => $this->sketch,
            'date_created' => $this->date_created,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'location', $this->location])
            ->andFilterWhere(['like', 'distance', $this->distance])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'area', $this->area])
            ->andFilterWhere(['like', 'photos', $this->photos])
            ->andFilterWhere(['like', 'notes', $this->notes]);

        $query->orderBy('site_id desc');

        return $dataProvider;
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sms_logs".
 *
 * @property int $id
 * @property string $message
 * @property string $phone
 * @property string $status
 * @property float $cost
 * @property int|null $batch_id
 * @property int|null $customer_id
 * @property string $date_sent
 *
 * @property SmsBatch $batch
 * @property Customers $customer
 */
class SmsLogs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sms_logs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['message', 'phone', 'status', 'cost'], 'required'],
            [['message'], 'string'],
            [['cost'], 'number'],
            [['batch_id', 'customer_id'], 'integer'],
            [['date_sent'], 'safe'],
            [['phone', 'status'], 'string', 'max' => 255],
            [['batch_id'], 'exist', 'skipOnError' => true, 'targetClass' => SmsBatch::className(), 'targetAttribute' => ['batch_id' => 'batch_id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customers::className(), 'targetAttribute' => ['customer_id' => 'customer_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'message' => 'Message',
            'phone' => 'Phone',
            'status' => 'Status',
            'cost' => 'Cost',
            'batch_id' => 'Batch ID',
            'customer_id' => 'Customer ID',
            'date_sent' => 'Date Sent',
        ];
    }

    /**
     * Gets query for [[Batch]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBatch()
    {
        return $this->hasOne(SmsBatch::className(), ['batch_id' => 'batch_id']);
    }

    /**
     * Gets query for [[Customer]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customers::className(), ['customer_id' => 'customer_id']);
    }
}

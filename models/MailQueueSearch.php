<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MailQueue;

/**
 * MailQueueSearch represents the model behind the search form of `app\models\MailQueue`.
 */
class MailQueueSearch extends MailQueue
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'attempts', 'sent'], 'integer'],
            [['sender_name', 'sender_email', 'email_to', 'cc', 'bcc', 'subject', 'html_body', 'text_body', 'reply_to', 'charset', 'created_at', 'last_attempt_time', 'sent_time', 'time_to_send', 'swift_message', 'attachments', 'email_template'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MailQueue::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'attempts' => $this->attempts,
            'last_attempt_time' => $this->last_attempt_time,
            'sent_time' => $this->sent_time,
            'time_to_send' => $this->time_to_send,
            'sent' => $this->sent,
        ]);

        $query->andFilterWhere(['like', 'sender_name', $this->sender_name])
            ->andFilterWhere(['like', 'sender_email', $this->sender_email])
            ->andFilterWhere(['like', 'email_to', $this->email_to])
            ->andFilterWhere(['like', 'cc', $this->cc])
            ->andFilterWhere(['like', 'bcc', $this->bcc])
            ->andFilterWhere(['like', 'subject', $this->subject])
            ->andFilterWhere(['like', 'html_body', $this->html_body])
            ->andFilterWhere(['like', 'text_body', $this->text_body])
            ->andFilterWhere(['like', 'reply_to', $this->reply_to])
            ->andFilterWhere(['like', 'charset', $this->charset])
            ->andFilterWhere(['like', 'swift_message', $this->swift_message])
            ->andFilterWhere(['like', 'attachments', $this->attachments])
            ->andFilterWhere(['like', 'email_template', $this->email_template]);

        return $dataProvider;
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "projects".
 *
 * @property int $project_id
 * @property string|null $name
 * @property string $description
 * @property string $location
 * @property string|null $start_date
 * @property string|null $end_date
 * @property float $amount_charged
 * @property int $status_id
 * @property int $user_id
 * @property int $customer_id
 * @property string $date_created
 * @property string|null $notes
 * @property string $estimated_area
 * @property float $actual_materials_cost
 * @property float $actual_labour_cost
 *
 * @property Documents[] $documents
 * @property Labour[] $labours
 * @property Materials[] $materials
 * @property MiscCosts[] $miscCosts
 * @property User $user
 * @property ProjectStatus $status
 * @property Customers $customer
 * @property Sites[] $sites
 */
class Projects extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'projects';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description', 'location', 'notes'], 'string'],
            [['description', 'location', 'amount_charged', 'status_id', 'user_id', 'customer_id', 'estimated_area'], 'required'],
            [['start_date', 'end_date', 'date_created'], 'safe'],
            [['amount_charged', 'actual_materials_cost', 'actual_labour_cost'], 'number'],
            [['status_id', 'user_id', 'customer_id'], 'integer'],
            [['estimated_area'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProjectStatus::className(), 'targetAttribute' => ['status_id' => 'status_id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customers::className(), 'targetAttribute' => ['customer_id' => 'customer_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'project_id' => 'Project ID',
            'name' => 'Name',
            'description' => 'Description',
            'location' => 'Location',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'amount_charged' => 'Amount Charged',
            'status_id' => 'Status ID',
            'user_id' => 'User ID',
            'customer_id' => 'Customer ID',
            'date_created' => 'Date Created',
            'notes' => 'Notes',
            'estimated_area' => 'Estimated Area',
            'actual_materials_cost' => 'Actual Materials Cost',
            'actual_labour_cost' => 'Actual Labour Cost',
        ];
    }

    /**
     * Gets query for [[Documents]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(Documents::className(), ['project_id' => 'project_id']);
    }

    /**
     * Gets query for [[Labours]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLabours()
    {
        return $this->hasMany(Labour::className(), ['project_id' => 'project_id']);
    }

    /**
     * Gets query for [[Materials]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMaterials()
    {
        return $this->hasMany(Materials::className(), ['project_id' => 'project_id']);
    }

    /**
     * Gets query for [[MiscCosts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMiscCosts()
    {
        return $this->hasMany(MiscCosts::className(), ['project_id' => 'project_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Gets query for [[Status]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(ProjectStatus::className(), ['status_id' => 'status_id']);
    }

    /**
     * Gets query for [[Customer]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customers::className(), ['customer_id' => 'customer_id']);
    }

    /**
     * Gets query for [[Sites]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSites()
    {
        return $this->hasMany(Sites::className(), ['project_id' => 'project_id']);
    }
}

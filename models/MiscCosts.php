<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "misc_costs".
 *
 * @property int $misc_cost_id
 * @property string $item
 * @property float $cost
 * @property int $project_id
 * @property string $date_created
 *
 * @property Projects $project
 */
class MiscCosts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'misc_costs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item', 'cost', 'project_id'], 'required'],
            [['item'], 'string'],
            [['cost'], 'number'],
            [['project_id'], 'integer'],
            [['date_created'], 'safe'],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Projects::className(), 'targetAttribute' => ['project_id' => 'project_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'misc_cost_id' => 'Misc Cost ID',
            'item' => 'Item',
            'cost' => 'Cost',
            'project_id' => 'Project ID',
            'date_created' => 'Date Created',
        ];
    }

    /**
     * Gets query for [[Project]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Projects::className(), ['project_id' => 'project_id']);
    }
}

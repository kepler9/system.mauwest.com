<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Materials;

/**
 * MaterialsSearch represents the model behind the search form of `app\models\Materials`.
 */
class MaterialsSearch extends Materials
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['materials_id', 'project_id', 'type'], 'integer'],
            [['item', 'unit', 'date_created'], 'safe'],
            [['quantity', 'rate', 'total'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        
        $query = Materials::find();

        // add conditions that should always apply here

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'materials_id' => $this->materials_id,
            'project_id' => $this->project_id,
            'quantity' => $this->quantity,
            'rate' => $this->rate,
            'total' => $this->total,
            'type' => $this->type,
            'date_created' => $this->date_created,
        ]);
        $query->andFilterWhere(['like', 'item', $this->item])
            ->andFilterWhere(['like', 'unit', $this->unit]);
        $query->orderBy('date_created desc');   

        if(\Yii::$app->request->get('download')){
            $query->orderBy('date_created asc');
            $file = \Yii::createObject([
                'class' => 'codemix\excelexport\ExcelFile',
                'sheets' => [
                    'Contacts' => [
                        'class' => 'codemix\excelexport\ActiveExcelSheet',
                        'query' => $query,
                        'attributes' => [
                            'item',
                            'quantity',
                            'unit',
                            'rate',
                            'total',
                            'date_created',
                        ],
                        'titles' => [
                            'A' => 'Item',
                            'B' => 'Quantity',
                            'C' => 'Unit',
                            'D' => 'Rate',
                            'E' => 'Total',
                            'F' => 'Date'
                        ],
                        'formats' => [
                            5 => 'dd/mm/yyyy',
                        ],
                        'formatters' => [
                            5 => function ($value, $row, $data) {
                                return \PHPExcel_Shared_Date::PHPToExcel(strtotime($value));
                            },
                        ],
                    ]
                ]
            ]);
            $type = $this->type == 1 ? 'Actual' : 'Estimated';
            $file->send($type.'-Materials-'.date('Y-m-d-H-i-s').'.xlsx');
            exit;
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        return $dataProvider;
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mail_queue".
 *
 * @property int $id
 * @property string|null $sender_name
 * @property string|null $sender_email
 * @property string|null $email_to
 * @property string|null $cc
 * @property string|null $bcc
 * @property string|null $subject
 * @property string|null $html_body
 * @property string|null $text_body
 * @property string|null $reply_to
 * @property string|null $charset
 * @property string $created_at
 * @property int|null $attempts
 * @property string|null $last_attempt_time
 * @property string|null $sent_time
 * @property string|null $time_to_send
 * @property string|null $swift_message
 * @property string|null $attachments
 * @property int $sent
 * @property string $email_template
 */
class MailQueue extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mail_queue';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sender_name', 'sender_email', 'email_to', 'cc', 'bcc', 'html_body', 'text_body', 'reply_to', 'swift_message', 'attachments'], 'string'],
            [['created_at', 'last_attempt_time', 'sent_time', 'time_to_send'], 'safe'],
            [['attempts', 'sent'], 'integer'],
            [['subject', 'charset', 'email_template'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sender_name' => 'Sender Name',
            'sender_email' => 'Sender Email',
            'email_to' => 'To',
            'cc' => 'Cc',
            'bcc' => 'Bcc',
            'subject' => 'Subject',
            'html_body' => 'Body',
            'text_body' => 'Text Body',
            'reply_to' => 'Reply To',
            'charset' => 'Charset',
            'created_at' => 'Date Queued',
            'attempts' => 'Attempts',
            'last_attempt_time' => 'Last Attempt Time',
            'sent_time' => 'Sent Time',
            'time_to_send' => 'Time To Send',
            'swift_message' => 'Swift Message',
            'attachments' => 'Attachments',
            'sent' => 'Sent',
            'email_template' => 'Email Template',
        ];
    }
}

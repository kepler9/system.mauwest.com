<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "casuals".
 *
 * @property int $casual_id
 * @property string $name
 * @property string|null $phone
 * @property string $date_created
 * @property float $daily_rate
 *
 * @property Labour[] $labours
 */
class Casuals extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'casuals';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'daily_rate'], 'required'],
            [['date_created'], 'safe'],
            [['daily_rate'], 'number'],
            [['name', 'phone'], 'string', 'max' => 255],
            [['phone'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'casual_id' => 'Casual ID',
            'name' => 'Name',
            'phone' => 'Phone',
            'date_created' => 'Date Created',
            'daily_rate' => 'Daily Rate',
        ];
    }

    /**
     * Gets query for [[Labours]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLabours()
    {
        return $this->hasMany(Labour::className(), ['casual_id' => 'casual_id']);
    }
}

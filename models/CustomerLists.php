<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customer_lists".
 *
 * @property int $list_id
 * @property string $name
 * @property string $type
 * @property string $date_created
 *
 * @property Customers[] $customers
 * @property SmsBatch[] $smsBatches
 */
class CustomerLists extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customer_lists';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['date_created'], 'safe'],
            [['name', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'list_id' => 'List ID',
            'name' => 'Name',
            'type' => 'Type',
            'date_created' => 'Date Created',
        ];
    }

    /**
     * Gets query for [[Customers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCustomers()
    {
        return $this->hasMany(Customers::className(), ['list_id' => 'list_id']);
    }

    /**
     * Gets query for [[SmsBatches]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSmsBatches()
    {
        return $this->hasMany(SmsBatch::className(), ['list_id' => 'list_id']);
    }
}

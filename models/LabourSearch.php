<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Labour;

/**
 * LabourSearch represents the model behind the search form of `app\models\Labour`.
 */
class LabourSearch extends Labour
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['labour_id', 'project_id', 'type'], 'integer'],
            [['quantity', 'rate', 'total'], 'number'],
            [['title', 'date_created'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {

        $query = Labour::find();

        // add conditions that should always apply here

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'labour_id' => $this->labour_id,
            'project_id' => $this->project_id,
            'quantity' => $this->quantity,
            'rate' => $this->rate,
            'total' => $this->total,
            'type' => $this->type,
            'date_created' => $this->date_created,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title]);
        $query->orderBy('title asc');

        if(\Yii::$app->request->get('download')){
            $query->orderBy('title asc');
            $file = \Yii::createObject([
                'class' => 'codemix\excelexport\ExcelFile',
                'sheets' => [
                    'Contacts' => [
                        'class' => 'codemix\excelexport\ActiveExcelSheet',
                        'query' => $query,
                        'attributes' => [
                            'title',
                            'date_created',
                            'rate',
                            'overtime',
                            'transport',
                            'wht',
                            'total',
                        ],
                        'titles' => [
                            'A' => 'Title',
                            'B' => 'Date',
                            'C' => 'Rate',
                            'D' => 'Overtime',
                            'E' => 'Transport',
                            'F' => 'WHT',
                            'G' => 'Total'
                        ],
                        'formats' => [
                            1 => 'dd/mm/yyyy',
                        ],
                        'formatters' => [
                            1 => function ($value, $row, $data) {
                                return \PHPExcel_Shared_Date::PHPToExcel(strtotime($value));
                            },
                        ],
                    ]
                ]
            ]);
            $type = $this->type == 1 ? 'Actual' : 'Estimated';
            $file->send($type.'-Labour-'.date('Y-m-d-H-i-s').'.xlsx');
            exit;
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        return $dataProvider;
    }
}

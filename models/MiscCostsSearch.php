<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MiscCosts;

/**
 * MiscCostsSearch represents the model behind the search form of `app\models\MiscCosts`.
 */
class MiscCostsSearch extends MiscCosts
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['misc_cost_id', 'project_id'], 'integer'],
            [['item', 'date_created'], 'safe'],
            [['cost'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = MiscCosts::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'misc_cost_id' => $this->misc_cost_id,
            'cost' => $this->cost,
            'project_id' => $this->project_id,
            'date_created' => $this->date_created,
        ]);

        $query->andFilterWhere(['like', 'item', $this->item]);
        $query->orderBy('date_created desc');

        if(\Yii::$app->request->get('download')){
            $query->orderBy('date_created asc');
            $file = \Yii::createObject([
                'class' => 'codemix\excelexport\ExcelFile',
                'sheets' => [
                    'Contacts' => [
                        'class' => 'codemix\excelexport\ActiveExcelSheet',
                        'query' => $query,
                        'attributes' => [
                            'item',
                            'cost',
                            'date_created',
                        ],
                        'titles' => [
                            'A' => 'Item',
                            'B' => 'Cost',
                            'C' => 'Date'
                        ],
                        'formats' => [
                            2 => 'dd/mm/yyyy',
                        ],
                        'formatters' => [
                            2 => function ($value, $row, $data) {
                                return \PHPExcel_Shared_Date::PHPToExcel(strtotime($value));
                            },
                        ],
                    ]
                ]
            ]);
            $file->send('Miscalleneous-Costs-'.date('Y-m-d-H-i-s').'.xlsx');
            exit;
        }

        return $dataProvider;
    }
}

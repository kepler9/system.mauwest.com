<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SmsBatch;

/**
 * SmsBatchSearch represents the model behind the search form of `app\models\SmsBatch`.
 */
class SmsBatchSearch extends SmsBatch
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['batch_id', 'list_id'], 'integer'],
            [['message', 'send_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SmsBatch::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'batch_id' => $this->batch_id,
            'send_date' => $this->send_date,
            'list_id' => $this->list_id,
        ]);
        
        $query->orderBy('batch_id desc');

        $query->andFilterWhere(['like', 'message', $this->message]);

        return $dataProvider;
    }
}

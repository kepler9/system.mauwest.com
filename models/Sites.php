<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sites".
 *
 * @property int $site_id
 * @property int|null $customer_id
 * @property int|null $project_id
 * @property string $date_visited
 * @property string $location
 * @property string $distance
 * @property float $fare
 * @property string $description
 * @property string $area
 * @property string|null $sketch
 * @property string|null $photos
 * @property string|null $notes
 * @property string $date_created
 * @property int|null $user_id
 *
 * @property Customers $customer
 * @property Projects $project
 * @property User $user
 */
class Sites extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sites';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id', 'project_id', 'user_id'], 'integer'],
            [['date_visited', 'location', 'distance', 'fare', 'description', 'area'], 'required'],
            [['date_visited', 'date_created'], 'safe'],
            [['location', 'description', 'sketch', 'photos', 'notes'], 'string'],
            [['fare'], 'number'],
            [['distance', 'area'], 'string', 'max' => 255],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customers::className(), 'targetAttribute' => ['customer_id' => 'customer_id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Projects::className(), 'targetAttribute' => ['project_id' => 'project_id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'site_id' => 'Site ID',
            'customer_id' => 'Customer ID',
            'project_id' => 'Project ID',
            'date_visited' => 'Date Visited',
            'location' => 'Location',
            'distance' => 'Distance',
            'fare' => 'Fare',
            'description' => 'Description',
            'area' => 'Area',
            'sketch' => 'Sketch',
            'photos' => 'Photos',
            'notes' => 'Notes',
            'date_created' => 'Date Created',
            'user_id' => 'User ID',
        ];
    }

    /**
     * Gets query for [[Customer]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customers::className(), ['customer_id' => 'customer_id']);
    }

    /**
     * Gets query for [[Project]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Projects::className(), ['project_id' => 'project_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}

<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Casuals;

/**
 * CasualsSearch represents the model behind the search form of `app\models\Casuals`.
 */
class CasualsSearch extends Casuals
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['casual_id'], 'integer'],
            [['name', 'phone', 'date_created'], 'safe'],
            [['daily_rate'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Casuals::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'casual_id' => $this->casual_id,
            'date_created' => $this->date_created,
            'daily_rate' => $this->daily_rate,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'phone', $this->phone]);

        return $dataProvider;
    }
}

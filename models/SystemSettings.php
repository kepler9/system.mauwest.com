<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "system_settings".
 *
 * @property int $id
 * @property string $settings
 * @property string|null $value
 * @property string|null $type
 * @property string $date_created
 */
class SystemSettings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'system_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['settings'], 'required'],
            [['date_created'], 'safe'],
            [['settings', 'value'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'settings' => 'Settings',
            'value' => 'Value',
            'type' => 'Type',
            'date_created' => 'Date Created',
        ];
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "measure_units".
 *
 * @property int $unit_id
 * @property string $name
 */
class MeasureUnits extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'measure_units';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'unit_id' => 'Unit ID',
            'name' => 'Name',
        ];
    }
}

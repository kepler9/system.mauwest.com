<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "labour".
 *
 * @property int $labour_id
 * @property int $project_id
 * @property float|null $wht
 * @property float|null $overtime
 * @property float|null $transport
 * @property float|null $quantity
 * @property float|null $rate
 * @property float $total
 * @property string|null $title
 * @property int $type
 * @property int|null $casual_id
 * @property string $date_created
 *
 * @property Projects $project
 * @property Casuals $casual
 */
class Labour extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'labour';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['project_id', 'total'], 'required'],
            [['project_id', 'type', 'casual_id'], 'integer'],
            [['wht', 'overtime', 'transport', 'quantity', 'rate', 'total'], 'number'],
            [['date_created'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Projects::className(), 'targetAttribute' => ['project_id' => 'project_id']],
            [['casual_id'], 'exist', 'skipOnError' => true, 'targetClass' => Casuals::className(), 'targetAttribute' => ['casual_id' => 'casual_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'labour_id' => 'Labour ID',
            'project_id' => 'Project ID',
            'wht' => 'Wht',
            'overtime' => 'Overtime',
            'transport' => 'Transport',
            'quantity' => 'Quantity',
            'rate' => 'Rate',
            'total' => 'Total',
            'title' => 'Title',
            'type' => 'Type',
            'casual_id' => 'Casual ID',
            'date_created' => 'Date Created',
        ];
    }

    /**
     * Gets query for [[Project]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Projects::className(), ['project_id' => 'project_id']);
    }

    /**
     * Gets query for [[Casual]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCasual()
    {
        return $this->hasOne(Casuals::className(), ['casual_id' => 'casual_id']);
    }
}

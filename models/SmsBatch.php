<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sms_batch".
 *
 * @property int $batch_id
 * @property string $message
 * @property string $send_date
 * @property int $list_id
 * @property int $processed
 *
 * @property CustomerLists $list
 * @property SmsLogs[] $smsLogs
 * @property SmsQueue[] $smsQueues
 */
class SmsBatch extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sms_batch';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['message', 'list_id'], 'required'],
            [['message'], 'string'],
            [['send_date'], 'safe'],
            [['list_id', 'processed'], 'integer'],
            [['list_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustomerLists::className(), 'targetAttribute' => ['list_id' => 'list_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'batch_id' => 'Batch ID',
            'message' => 'Message',
            'send_date' => 'Send Date',
            'list_id' => 'List ID',
            'processed' => 'Processed',
        ];
    }

    /**
     * Gets query for [[List]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getList()
    {
        return $this->hasOne(CustomerLists::className(), ['list_id' => 'list_id']);
    }

    /**
     * Gets query for [[SmsLogs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSmsLogs()
    {
        return $this->hasMany(SmsLogs::className(), ['batch_id' => 'batch_id']);
    }

    /**
     * Gets query for [[SmsQueues]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSmsQueues()
    {
        return $this->hasMany(SmsQueue::className(), ['batch_id' => 'batch_id']);
    }
}

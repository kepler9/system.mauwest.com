<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "documents".
 *
 * @property int $document_id
 * @property int $project_id
 * @property int $document_type_id
 * @property string|null $file
 * @property string|null $date_sent
 * @property string $date_created
 *
 * @property Projects $project
 * @property DocumentTypes $documentType
 */
class Documents extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'documents';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['project_id', 'document_type_id'], 'required'],
            [['project_id', 'document_type_id'], 'integer'],
            [['file'], 'string'],
            [['date_sent', 'date_created'], 'safe'],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Projects::className(), 'targetAttribute' => ['project_id' => 'project_id']],
            [['document_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => DocumentTypes::className(), 'targetAttribute' => ['document_type_id' => 'document_type_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'document_id' => 'Document ID',
            'project_id' => 'Project ID',
            'document_type_id' => 'Document Type ID',
            'file' => 'File',
            'date_sent' => 'Date Sent',
            'date_created' => 'Date Created',
        ];
    }

    /**
     * Gets query for [[Project]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Projects::className(), ['project_id' => 'project_id']);
    }

    /**
     * Gets query for [[DocumentType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentType()
    {
        return $this->hasOne(DocumentTypes::className(), ['document_type_id' => 'document_type_id']);
    }
}

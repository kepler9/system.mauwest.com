<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'name' => 'Mau West Limited',
    'defaultRoute' => 'welcome/index',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'formatter' => [
             'thousandSeparator' => ',',
             'currencyCode' => 'KSh',
        ],
        'assetManager' => [
            'bundles' => [
                // BootstrapAsset
                'yii\bootstrap\BootstrapAsset' => [
                    'sourcePath' => NULL,
                    'css' => [
                        'assets/vendors/bootstrap/dist/css/bootstrap.min.css'
                    ],
                ],
                // BootstrapPluginAsset
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'sourcePath' => NULL,
                    'js' => [
                        'assets/vendors/bootstrap/dist/js/bootstrap.min.js'
                    ],
                ],
                // JqueryAsset
                'yii\web\JqueryAsset' => [
                    'sourcePath' => NULL,
                    'js' => [
                        "assets/vendors/jquery/dist/jquery.min.js",
                    ],
                    'jsOptions' => [ 'position' => \yii\web\View::POS_HEAD ],
                ],
            ],
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'QYxy-gyILUkK6L0bfYj-F0A8DZO8vGTA',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'gator3172.hostgator.com',
                'username' => 'app@mauwest.com',
                'password' => 'Do6bg01%',
                'port' => '465',
                'encryption' => 'ssl'
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;

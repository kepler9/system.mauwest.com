<?php
return [
    'adminEmail' => 'admin@mauwest.com',
    'senderEmail' => 'app@mauwest.com',
    'senderName' => 'Mau West System',
    'company' => 'MAU WEST LTD',
    'abbr' => 'MW',
];

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MailQueue */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mail-queue-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'sender_name')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'sender_email')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'email_to')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'cc')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'bcc')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'html_body')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'text_body')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'reply_to')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'charset')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'attempts')->textInput() ?>

    <?= $form->field($model, 'last_attempt_time')->textInput() ?>

    <?= $form->field($model, 'sent_time')->textInput() ?>

    <?= $form->field($model, 'time_to_send')->textInput() ?>

    <?= $form->field($model, 'swift_message')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'attachments')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'sent')->textInput() ?>

    <?= $form->field($model, 'email_template')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MailQueue */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Mail Queues', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="mail-queue-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'sender_name:ntext',
            'sender_email:ntext',
            'email_to:ntext',
            'cc:ntext',
            'bcc:ntext',
            'subject',
            'html_body:ntext',
            'text_body:ntext',
            'reply_to:ntext',
            'charset',
            'created_at',
            'attempts',
            'last_attempt_time',
            'sent_time',
            'time_to_send',
            'swift_message:ntext',
            'attachments:ntext',
            'sent',
            'email_template:email',
        ],
    ]) ?>

</div>

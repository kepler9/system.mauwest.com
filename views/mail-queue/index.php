<?php
use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Mail Queue';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <?=Html::a('<b>CLEAR</b>',['mail-queue/clear'],['class'=>'btn btn-danger btn-sm']);?>
        </div>
    </div>
    <div class="ibox-body">
        <div class="table-responsive">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                // 'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'email_to:email',
                    'bcc:email',
                    'subject',
                    'html_body:html',
                    'created_at:date',
                    ['class' => 'yii\grid\ActionColumn','template'=>'{delete}'],
                ],
            ]); ?>
        </div>
    </div>
</div>

<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
$this->title = $model->customer->name;
$this->params['breadcrumbs'][] = ['label' => 'Site Visits', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title." - ".$model->description?></div>
        <div class="ibox-tools">
            <?= !$model->project_id ? Html::a('Create Project', ['projects/create', 'site_id' => $model->site_id], ['class' => 'btn btn-dark btn-sm']) : ""; ?>
            <?= Html::a('Update', ['update', 'id' => $model->site_id], ['class' => 'btn btn-primary btn-sm']) ?>
            <?= Html::a('<span style="color:#777;font-weight:bold">Delete</span>', ['delete', 'id' => $model->site_id], [
                'class' => 'btn btn-secondary btn-sm',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </div>
    </div>
    <div class="ibox-body">
        <div class="sites-view">

            <?=$this->render('_view',['model'=>$model]);?>

        </div>
    </div>
</div>

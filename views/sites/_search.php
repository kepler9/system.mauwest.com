<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SitesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sites-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'site_id') ?>

    <?= $form->field($model, 'customer_id') ?>

    <?= $form->field($model, 'project_id') ?>

    <?= $form->field($model, 'date_visited') ?>

    <?= $form->field($model, 'location') ?>

    <?php // echo $form->field($model, 'distance') ?>

    <?php // echo $form->field($model, 'fare') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'area') ?>

    <?php // echo $form->field($model, 'measurements') ?>

    <?php // echo $form->field($model, 'sketch') ?>

    <?php // echo $form->field($model, 'photos') ?>

    <?php // echo $form->field($model, 'notes') ?>

    <?php // echo $form->field($model, 'date_created') ?>

    <?php // echo $form->field($model, 'user_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

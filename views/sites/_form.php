<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use yii\helpers\ArrayHelper;
    use kartik\select2\Select2;
?>
<div class="sites-form">

    <?php $form = ActiveForm::begin(); ?>

    <label>Customer | <a href="/customers/create?redirect=<?=Yii::$app->request->url?>"><i class="fa fa-plus"></i> Add</a></label>
    <?=$form->field($model,'customer_id')->widget(Select2::classname(), 
        [
            'data' => ArrayHelper::map(\app\models\Customers::find()->orderBy('customer_id desc')->asArray()->all(),'customer_id','name'),
            'options' => ['placeholder' => 'Select'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]
    )->label(false);?>

    <?= $form->field($model, 'date_visited')->textInput(['type'=>'date','max'=>date('Y-m-d')]) ?>

    <?= $form->field($model, 'location')->textarea(['rows' => 1])->label('Site Location') ?>

    <?= $form->field($model, 'distance')->textInput(['maxlength' => true])->label('Distance from NRB to the Site') ?>

    <?= $form->field($model, 'fare')->textInput()->label('Approx. Fare / Person to the Site') ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 3])->label('Job Description') ?>

    <?= $form->field($model, 'notes')->textarea(['rows' => 3])->label('Additional Notes')->hint('e.g equipments to be hired') ?>

    <?= $form->field($model, 'area')->textInput(['maxlength' => true]) ?>
    
    <label>Upload a Sketch / Measurements</label>
    <?= $form->field($model, 'sketch')->fileInput(['name'=>'sketch','accept'=>'.png,.jpg'])->label(false) ?>
    <hr>

    <label>Upload Photos</label>
    <?= $form->field($model, 'photos')->fileInput(['name' => 'photos[]','multiple'=>'true','accept'=>'.png,.jpg'])->label(false) ?>   
    <hr><br>
    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-success btn-sm pull-right']) ?>
        <span class="clearfix"></span>
    </div>
    <?php ActiveForm::end(); ?>
</div>

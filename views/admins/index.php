<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Admins';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <?= Html::a('Create Admin Account', ['create'], ['class' => 'btn btn-success btn-sm']) ?>
        </div>
    </div>
    <div class="ibox-body">
        <div class="table-responsive">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                // 'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'username',
                    'email:email',
                    'phone_no',
                    'date_created:date',
                    ['attribute' => 'accountStatus', 'format' => 'raw'],
                    ['attribute' => 'notificationsStatus', 'format' => 'raw'],
                    ['class' => 'yii\grid\ActionColumn','template'=>'{view}'],
                ],
            ]); ?>
        </div>
    </div>
</div>

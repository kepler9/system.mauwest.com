<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ProjectStatus */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Project Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <?= Html::a('Update', ['update', 'id' => $model->status_id], ['class' => 'btn btn-primary btn-sm']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->status_id], [
                'class' => 'btn btn-danger btn-sm',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </div>
    </div>
    <div class="ibox-body">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'status_id',
                'name',
                'alert',
                'description:ntext',
            ],
        ]) ?>
    </div>
</div>
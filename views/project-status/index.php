<?php
    use yii\helpers\Html;
    use yii\grid\GridView;
    $this->title = 'Project Statuses';
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <?= Html::a('Create Project Status', ['create'], ['class' => 'btn btn-success btn-sm']) ?>
        </div>
    </div>
    <div class="ibox-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            // 'filterModel' => $searchModel,
            'columns' => [
                // ['class' => 'yii\grid\SerialColumn'],
                'status_id',
                'name',
                'alert',
                'description:ntext',
                ['class' => 'yii\grid\ActionColumn','template'=>'{view}'],
            ],
        ]); ?>
    </div>
</div>
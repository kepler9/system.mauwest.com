<?php
    use yii\helpers\Html;
    use yii\widgets\DetailView;
    use yii\grid\GridView;
    $this->title = $model->description;
    $this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
    \yii\web\YiiAsset::register($this);
    $actual_costs = $this->context->model->actual_costs($model->project_id);
    $estimated_costs = $this->context->model->estimated_costs($model->project_id);
?>
<div class="row">
    <div class="col-md-3">
        <div class="ibox bg-light-dark text-white">
            <div class="ibox-body">
                <h2 class="mb-1"><?=number_format($this->context->model->estimated_costs($model->project_id))?></h2>
                <div class="text-white">Estimated Costs</div><i class="la la-money widget-stat-icon"></i>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="ibox bg-info text-white">
            <div class="ibox-body">
                <h2 class="mb-1"><?=number_format($this->context->model->actual_costs($model->project_id))?></h2>
                <div class="text-white">Actual Costs</div><i class="la la-money widget-stat-icon"></i>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="ibox bg-dark text-white">
            <div class="ibox-body">
                <h2 class="mb-1"><?=$estimated_costs ? number_format($model->amount_charged - $estimated_costs) : 0?></h2>
                <div class="text-white">Estimated Profit</div><i class="la la-money widget-stat-icon"></i>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="ibox bg-pink text-white">
            <div class="ibox-body">
                <h2 class="mb-1"><?=$actual_costs ? number_format($model->amount_charged - $actual_costs) : 0?></h2>
                <div class="text-white">Actual Profit</div><i class="la la-money widget-stat-icon"></i>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">About Project</div>
                <div class="ibox-tools">
                    <?php if(!in_array($model->status_id, [4,5])):?>
                        <?= Html::a('Update Details', ['update', 'id' => $model->project_id], ['class' => 'btn btn-primary btn-sm']) ?>
                        <?= Html::a('Close Project', ['update', 'id' => $model->project_id,'type'=>'close'], ['class' => 'btn btn-danger btn-sm']) ?>
                    <?php endif;?>
                </div>
            </div>
            <div class="ibox-body">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" href="#project-details" data-toggle="tab">Project Details</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#project-documents" data-toggle="tab">Project Documents</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#estimated-costs" data-toggle="tab">Estimated Costs</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#actual-costs" data-toggle="tab">Actual Costs</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#site-visit" data-toggle="tab">Site Visit</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="project-details">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'description:ntext',
                                ['attribute'=>'amount_charged','format'=>'raw','value'=>'<span class="badge badge-danger"><b>'.number_format($model->amount_charged).'</b></span>','label'=>'Amount Quoted'],
                                'location:ntext',
                                'estimated_area',
                                'start_date:date',
                                'end_date:date',
                                ['label'=>'Estimated Duration','attribute'=>'start_date','value'=>function($model){
                                    return $this->context->model->project_duration($model->project_id);
                                },'format'=>'raw'],
                                ['label'=>'Actual Duration','attribute'=>'start_date','value'=>function($model){
                                    return $this->context->model->actual_project_duration($model->project_id);
                                },'format'=>'raw'],
                                ['attribute'=>'status_id','format'=>'raw','value'=>function($model){
                                    return "<span data-toggle='tooltip' data-trigger='hover' data-placement='top' data-original-title='".$model->status->description."' class='badge badge-".$model->status->alert."'>".$model->status->name."</span>";
                                },'label'=>'Status','filter'=>false],
                                ['attribute'=>'customer_id','label'=>'Customer','format'=>'raw','value'=>function($model){
                                    return $model->customer ? Html::a($model->customer->name,['customers/view','id'=>$model->customer_id,'project_id'=>$model->project_id]) : NULL;
                                }],
                                ['attribute'=>'phone','format'=>'raw','value'=>function($model){
                                    return $model->customer ? Html::a($model->customer->phone,['customers/message','customer_id'=>$model->customer_id,'redirect'=>Yii::$app->request->url]) : NULL;
                                }],
                                'date_created:date',
                                ['attribute'=>'user.username','label'=>'Created By'],
                                'notes'
                            ],
                        ]) ?>
                        <?php if(!in_array($model->status_id, [4])):?>
                            <?= Html::a('<span style="color:#777;font-weight:bold">Delete Project</span>', ['delete', 'id' => $model->project_id], [
                                'class' => 'btn btn-secondary btn-sm pull-right',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this project?',
                                    'method' => 'post',
                                ],
                            ]) ?>
                            <span class="clearfix"></span>
                        <?php endif;?>
                    </div>
                    <div class="tab-pane fade" id="project-documents">
                        <?php if(!in_array($model->status_id, [4,5])):?>
                            <?=Html::a('<i class="fa fa-edit"></i> Update',['update-documents','project_id'=>$model->project_id],['class'=>'btn btn-outline-blue btn-fix btn-rounded btn-sm']);?>
                            <br><br>
                        <?php endif;?>
                        <?= GridView::widget([
                            'dataProvider' => $documentsDataProvider,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                'documentType.name',
                                ['attribute'=>'file','label'=>'Document'],
                                'date_created:date',
                                'date_sent:date',
                                ['class' => 'yii\grid\ActionColumn','template'=> '{update} {view} {delete}','buttons'=>[
                                    'update' => function($url,$model,$id){
                                        return Html::a('<i class="fa fa-edit"></i>',['edit-document','id'=>$id]);
                                    },
                                    'view' => function($url,$model,$id){
                                        return Html::a('| <i class="fa fa-download"></i>',['download-document','id'=>$id]);
                                    },
                                    'delete' => function($url,$model,$id){
                                        if(!in_array($model->project->status_id, [4,5])){
                                            return Html::a('| <i class="fa fa-trash"></i>',['delete-document','id'=>$id],['data-confirm'=>'Are you sure you want to delete this document?']);
                                        }
                                    }
                                ]],
                            ],
                        ]) ?>
                    </div>
                    <div class="tab-pane fade" id="estimated-costs">
                        <div class="boxed" id="accordion-estimated-costs">
                            <!-- estimated materials -->
                            <div class="card">
                                <div class="card-header" id="heading-estimated-materials">
                                  <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapse-estimated-materials" aria-expanded="true" aria-controls="collapseOne">
                                      Estimated Materials
                                    </button>
                                  </h5>
                                </div>
                                <div id="collapse-estimated-materials" class="collapse show" aria-labelledby="heading-estimated-materials" data-parent="#accordion-estimated-costs">
                                  <div class="card-body">
                                    <?php if(!in_array($model->status_id, [4,5])):?>
                                    <p class="pull-left"><?=Html::a('<i class="fa fa-plus-circle"></i> Add',['materials/create','type'=>0],['class'=>'btn btn-outline-blue btn-fix btn-rounded btn-sm']);?></p>
                                    <?php endif;?>
                                    <div class="btn-group btn-rounded pull-right">
                                        <div class="btn btn-outline-secondary btn-sm"><?=Html::a('Total KSh <b>'.number_format($this->context->model->estimated_materials_cost($model->project_id)).'</b>',['materials/index','type'=>0]);?></div>
                                        <div class="btn btn-outline-secondary btn-sm"><?=Html::a('<i class="fa fa-download"></i> Download',['materials/index','type'=>0,'download'=>1]);?></div>
                                    </div>
                                    <span class="clearfix"></span>
                                    <div class="table-responsive">
                                        <?=GridView::widget([
                                            'dataProvider' => $this->context->model->estimated_materials($model->project_id,'grid'),
                                            'columns' => [
                                                ['class' => 'yii\grid\SerialColumn'],
                                                'item:ntext',
                                                ['attribute'=>'quantity','filter'=>false,'value'=>function($model){
                                                    return number_format($model->quantity).' '.$model->unit;
                                                }],
                                                'rate:decimal',
                                                'total:decimal',
                                                ['attribute'=>'date_created','format'=>'date','label'=>'Date'],
                                                ['class' => 'yii\grid\ActionColumn','template' => '{update} {delete}','buttons'=>[
                                                    'update' => function($url,$model,$id){
                                                        return !in_array($model->project->status_id, [4,5]) ? Html::a('<i class="fa fa-pencil"></i>',['materials/update','id'=>$id,'redirect'=>Yii::$app->request->url]) : false;
                                                    },
                                                    'delete' => function($url,$model,$id){
                                                        return !in_array($model->project->status_id, [4,5]) ? Html::a('<i class="fa fa-trash"></i>',['materials/delete','id'=>$id]) : false;
                                                    }
                                                ]],
                                            ],
                                        ]);?>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <!-- estimated labour -->
                            <div class="card">
                                <div class="card-header" id="heading-estimated-labour">
                                  <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapse-estimated-labour" aria-expanded="true" aria-controls="collapse-estimated-labour">
                                      Estimated Labour
                                    </button>
                                  </h5>
                                </div>
                                <div id="collapse-estimated-labour" class="collapse" aria-labelledby="heading-estimated-labour" data-parent="#accordion-estimated-costs">
                                  <div class="card-body">
                                    <?php if(!in_array($model->status_id, [4,5])):?>
                                    <p class="pull-left"><?=Html::a('<i class="fa fa-plus-circle"></i> Add',['labour/create','type'=>0],['class'=>'btn btn-outline-blue btn-fix btn-rounded btn-sm']);?></p>
                                    <?php endif;?>
                                    <div class="btn-group btn-rounded pull-right">
                                        <div class="btn btn-outline-secondary btn-sm"><?=Html::a('Total KSh <b>'.number_format($this->context->model->estimated_labour_cost($model->project_id)).'</b>',['labour/index','type'=>0]);?></div>
                                        <div class="btn btn-outline-secondary btn-sm"><?=Html::a('<i class="fa fa-download"></i> Download',['labour/index','type'=>0,'download'=>1]);?></div>
                                    </div>
                                    <span class="clearfix"></span>
                                    <br>
                                    <div class="table-responsive">
                                        <?=GridView::widget([
                                            'dataProvider' => $this->context->model->estimated_labour($model->project_id,'grid'),
                                            'columns' => [
                                                ['class'=>'yii\grid\SerialColumn'],
                                                'title',
                                                ['attribute'=>'wht','format'=>'decimal','filter'=>false,'label'=>'WHT'],
                                                ['attribute'=>'overtime','format'=>'decimal','filter'=>false],
                                                ['attribute'=>'transport','format'=>'decimal','filter'=>false],
                                                ['attribute'=>'rate','format'=>'decimal','filter'=>false],
                                                ['attribute'=>'total','format'=>'decimal','filter'=>false],
                                                ['attribute'=>'date_created','format'=>'date','label'=>'Date'],
                                                ['class' => 'yii\grid\ActionColumn','template'=>'{update} {delete}','buttons'=>[
                                                    'delete' => function($url,$model,$id){
                                                        return !in_array($model->project->status_id, [4,5]) ? Html::a('<i class="fa fa-trash"></i>',['labour/delete','id'=>$id]) : false;
                                                    },
                                                    'update' => function($url,$model,$id){
                                                        return !in_array($model->project->status_id, [4,5]) ? Html::a('<i class="fa fa-pencil"></i>',['labour/update','id'=>$id,'redirect'=>Yii::$app->request->url]) : false;
                                                    },
                                                ]],
                                            ],
                                        ]);?>
                                    </div>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="actual-costs">
                        <div class="boxed" id="accordion-actual-costs">
                            <div class="card">
                                <div class="card-header" id="heading-actual-materials">
                                  <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapse-actual-materials" aria-expanded="true" aria-controls="collapse-actual-materials">
                                      Actual Materials
                                    </button>
                                  </h5>
                                </div>
                                <div id="collapse-actual-materials" class="collapse show" aria-labelledby="heading-actual-materials" data-parent="#accordion-actual-costs">
                                  <div class="card-body">
                                    <?php if(!in_array($model->status_id, [4,5])):?>
                                    <p class="pull-left"><?=Html::a('<i class="fa fa-plus-circle"></i> Add',['materials/create','type'=>1],['class'=>'btn btn-outline-blue btn-fix btn-rounded btn-sm']);?></p>
                                    <?php endif;?>
                                    <div class="btn-group btn-rounded pull-right">
                                        <div class="btn btn-outline-secondary btn-sm"><?=Html::a('Total KSh <b>'.number_format($this->context->model->actual_materials_cost($model->project_id)).'</b>',['materials/index','type'=>1]);?></div>
                                        <div class="btn btn-outline-secondary btn-sm"><?=Html::a('<i class="fa fa-download"></i> Download',['materials/index','type'=>1,'download'=>1]);?></div>
                                    </div>
                                    <span class="clearfix"></span>
                                    <br>
                                    <div class="table-responsive">
                                        <?=GridView::widget([
                                            'dataProvider' => $this->context->model->actual_materials($model->project_id,'grid'),
                                            'columns' => [
                                                ['class' => 'yii\grid\SerialColumn'],
                                                'item:ntext',
                                                ['attribute'=>'quantity','filter'=>false,'value'=>function($model){
                                                    return number_format($model->quantity).' '.$model->unit;
                                                }],
                                                'rate:decimal',
                                                'total:decimal',
                                                ['attribute'=>'date_created','format'=>'date','label'=>'Date'],
                                                ['class' => 'yii\grid\ActionColumn','template' => '{update} {delete}','buttons'=>[
                                                    'update' => function($url,$model,$id){
                                                        return !in_array($model->project->status_id, [4,5]) ? Html::a('<i class="fa fa-pencil"></i>',['materials/update','id'=>$id,'redirect'=>Yii::$app->request->url]) : false;
                                                    },
                                                    'delete' => function($url,$model,$id){
                                                        return !in_array($model->project->status_id, [4,5]) ? Html::a('<i class="fa fa-trash"></i>',['materials/delete','id'=>$id]) : false;
                                                    }
                                                ]],
                                            ],
                                        ]);?>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="heading-actual-labour">
                                  <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapse-actual-labour" aria-expanded="true" aria-controls="collapse-actual-labour">
                                      Actual Labour
                                    </button>
                                  </h5>
                                </div>
                                <div id="collapse-actual-labour" class="collapse" aria-labelledby="heading-actual-labour" data-parent="#accordion-actual-costs">
                                  <div class="card-body">
                                    <?php if(!in_array($model->status_id, [4,5])):?>
                                    <p class="pull-left"><?=Html::a('<i class="fa fa-plus-circle"></i> Add',['labour/create','type'=>1],['class'=>'btn btn-outline-blue btn-fix btn-rounded btn-sm']);?></p>
                                    <?php endif;?>
                                    <div class="btn-group btn-rounded pull-right">
                                        <div class="btn btn-outline-secondary btn-sm"><?=Html::a('Total KSh <b>'.number_format($this->context->model->actual_labour_cost($model->project_id)).'</b>',['labour/index','type'=>1]);?></div>
                                        <div class="btn btn-outline-secondary btn-sm"><?=Html::a('<i class="fa fa-download"></i> Download',['labour/index','type'=>1,'download'=>1]);?></div>
                                    </div>
                                    <span class="clearfix"></span>
                                    <br>
                                    <div class="table-responsive">
                                        <?=GridView::widget([
                                            'dataProvider' => $this->context->model->actual_labour($model->project_id,'grid'),
                                            'columns' => [
                                                ['class'=>'yii\grid\SerialColumn'],
                                                'title',
                                                ['attribute'=>'wht','format'=>'decimal','filter'=>false,'label'=>'WHT'],
                                                ['attribute'=>'overtime','format'=>'decimal','filter'=>false],
                                                ['attribute'=>'transport','format'=>'decimal','filter'=>false],
                                                ['attribute'=>'rate','format'=>'decimal','filter'=>false],
                                                ['attribute'=>'total','format'=>'decimal','filter'=>false],
                                                ['attribute'=>'date_created','format'=>'date','label'=>'Date'],
                                                ['class' => 'yii\grid\ActionColumn','template'=>'{update} {delete}','buttons'=>[
                                                    'delete' => function($url,$model,$id){
                                                        return !in_array($model->project->status_id, [4,5]) ? Html::a('<i class="fa fa-trash"></i>',['labour/delete','id'=>$id]) : false;
                                                    },
                                                    'update' => function($url,$model,$id){
                                                        return !in_array($model->project->status_id, [4,5]) ? Html::a('<i class="fa fa-pencil"></i>',['labour/update','id'=>$id,'redirect'=>Yii::$app->request->url]) : false;
                                                    },
                                                ]],
                                            ],
                                        ]);?>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="heading-misc">
                                  <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapse-misc" aria-expanded="true" aria-controls="collapse-misc">
                                      Miscellaneous
                                    </button>
                                  </h5>
                                </div>
                                <div id="collapse-misc" class="collapse" aria-labelledby="heading-misc" data-parent="#accordion-actual-costs">
                                  <div class="card-body">
                                    <?php if(!in_array($model->status_id, [4,5])):?>
                                    <p class="pull-left"><?=Html::a('<i class="fa fa-plus-circle"></i> Add',['misc-costs/create'],['class'=>'btn btn-outline-blue btn-fix btn-rounded btn-sm']);?></p>
                                    <?php endif;?>
                                    <div class="btn-group btn-rounded pull-right">
                                        <div class="btn btn-outline-secondary btn-sm"><?=Html::a('Total KSh <b>'.number_format($this->context->model->misc_costs($model->project_id)).'</b>',['misc-costs/index','project_id'=>$model->project_id]);?></div>
                                        <div class="btn btn-outline-secondary btn-sm"><?=Html::a('<i class="fa fa-download"></i> Download',['misc-costs/index','download'=>1,'project_id'=>$model->project_id]);?></div>
                                    </div>
                                    <span class="clearfix"></span>
                                    <br>
                                    <div class="table-responsive">
                                        <?=GridView::widget([
                                            'dataProvider' => $this->context->model->miscellaneous_costs($model->project_id,'grid'),
                                            'columns' => [
                                                ['class'=>'Yii\grid\SerialColumn'],
                                                'item:ntext',
                                                'cost:decimal',
                                                ['attribute'=>'date_created','label'=>'Date','format'=>'date'],
                                                ['class' => 'yii\grid\ActionColumn','template'=>'{update} {delete}','buttons'=>[
                                                    'update' => function($url,$model,$id){
                                                        return !in_array($model->project->status_id, [4,5]) ? Html::a('<i class="fa fa-pencil"></i>',['misc-costs/update','id'=>$id,'redirect'=>Yii::$app->request->url]) : false;
                                                    },
                                                    'delete' => function($url,$model,$id){
                                                        return !in_array($model->project->status_id, [4,5]) ? Html::a('<i class="fa fa-trash"></i>',['misc-costs/delete','id'=>$id,'redirect'=>Yii::$app->request->url]) : false;
                                                    }
                                                ]],
                                            ],
                                        ]);?>
                                    </div>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="site-visit">
                        <?php $site = \app\models\Sites::findOne(['project_id'=>$model->project_id]); ?>
                        <?= $site ? $this->render('//sites/_view',['model'=> $site]) : ""; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
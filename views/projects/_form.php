<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
?>
<div class="projects-form">
    <?php $form = ActiveForm::begin(); ?>
    <?php if(isset($type) && $type == 'close'):?>
        <?= $form->field($model, 'status_id')->dropDownList(ArrayHelper::map(\app\models\ProjectStatus::find()->where(['status_id'=>[4,5,7]])->all(),'status_id','name'),['data-validation'=>'required','prompt'=>'Select'])->label('Status') ?>
        <?=$form->field($model,'notes')->textarea(['placeholder'=>'Enter a brief comment on the project']);?>
    <?php else:?>
        <label>Customer | <a href="/customers/create?redirect=<?=Yii::$app->request->url?>"><i class="fa fa-plus"></i> Add</a></label>
        <?=$form->field($model,'customer_id')->widget(Select2::classname(), 
            [
                'data' => ArrayHelper::map(\app\models\Customers::find()->orderBy('customer_id desc')->asArray()->all(),'customer_id','name'),
                'options' => ['placeholder' => 'Select'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]
        )->label(false);?>
        <?= $form->field($model, 'description')->textarea(['rows' => 2,'placeholder'=>'Enter project title or description'])->label('Description') ?>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'status_id')->dropDownList(ArrayHelper::map(\app\models\ProjectStatus::find()->where(['status_id'=>[1,2,3,6]])->all(),'status_id','name'),['prompt'=>'Select'])->label('Status') ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'location')->textarea(['rows' => 1,'placeholder'=>'Enter project location']) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'estimated_area')->textInput(['placeholder' => 'Enter estimated area']) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'amount_charged')->textInput(['placeholder' => 'Enter project amount']) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'start_date')->textInput(['type'=>'date']) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'end_date')->textInput(['type'=>'date']) ?>
            </div>
        </div>
    <?php endif;?>
    <hr class="hr-25">
    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-success btn-sm pull-right']) ?>
        <span class="clearfix"></span>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<script type="text/javascript">
    $(function(){
        $('#projects-description').keyup(function(event) {
            $('#projects-name').val($(this).val());
        });
    });
</script>

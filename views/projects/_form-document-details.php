<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = 'Update Document Details';
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->project->name, 'url' => ['view', 'id' => $model->project_id]];
$this->params['breadcrumbs'][] = $this->title;
$model->date_sent = $model->date_sent ? date('Y-m-d',strtotime($model->date_sent)) : NULL;
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
        </div>
    </div>
    <div class="ibox-body">
    	<div class="mail-queue-form">

		    <?php $form = ActiveForm::begin(); ?>
		    	<?=$form->field($model,'date_sent')->textInput(['type'=>'date']);?>
		    	<div class="form-group pull-right">
			        <?= Html::submitButton('Save', ['class' => 'btn btn-sm btn-success']) ?>
			    </div>
			    <span class="clearfix"></span>
		    <?php ActiveForm::end();?>

		</div>
    </div>
</div>

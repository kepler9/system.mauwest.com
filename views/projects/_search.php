<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use yii\helpers\ArrayHelper;
    use kartik\select2\Select2;
?>
<div class="projects-search">
    <?php $form = ActiveForm::begin(['action' => ['index'],'enableClientValidation'=>false,'method' => 'get','options'=>['class'=>'form-horizontal']]); ?>
    <div class="row">
        <div class="col-md-3">
            <?php  echo $form->field($model, 'status_id')->dropDownList(ArrayHelper::map(\app\models\ProjectStatus::find()->all(),'status_id','name'),['prompt'=>'Project Status','name'=>'status_id','style'=>'height:35px;'])->label(false) ?>
        </div>
        <div class="col-md-3">
            <?=$form->field($model,'customer_id')->widget(Select2::classname(), 
                [
                    'data' => ArrayHelper::map(\app\models\Customers::find()->orderBy('customer_id desc')->asArray()->all(),'customer_id','name'),
                    'options' => ['placeholder' => 'Select Customer','name'=>'customer_id'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]
            )->label(false);?>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary btn-sm','style'=>'height:33px']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<br>
<?php
    use yii\helpers\Html;
    use yii\grid\GridView;
    $this->title = 'Projects';
    $this->params['breadcrumbs'][] = ['label'=>$this->title,'url'=>['index']];
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?> <span style="position: relative;top:-5px;" class="badge badge-danger badge-danger"><?=$dataProvider->totalCount;?></span></div>
        <div class="ibox-tools">
            <?= Html::a('<i class="fa fa-plus"></i> New Project', ['create'], ['class' => 'btn btn-success btn-sm']) ?>
        </div>
    </div>
    <div class="ibox-body">
        
        <?=$this->render('_search',['model'=>$model]);?>
        
        <div class="table-responsive">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                // 'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    ['attribute'=>'description'],
                    ['attribute'=>'start_date','format'=>'date','filter'=>false],
                    ['attribute'=>'end_date','format'=>'date','filter'=>false],
                    ['label'=>'Estimated Duration','attribute'=>'start_date','value'=>function($model){
                        return $this->context->model->project_duration($model->project_id);
                    },'format'=>'raw'],
                    ['label'=>'Actual Duration','attribute'=>'start_date','value'=>function($model){
                        return $this->context->model->actual_project_duration($model->project_id);
                    },'format'=>'raw'],
                    'estimated_area',
                    ['attribute'=>'status_id','format'=>'raw','value'=>function($model){
                        return "<span data-toggle='tooltip' data-trigger='hover' data-placement='top' data-original-title='".$model->status->description."' class='badge badge-".$model->status->alert."'>".$model->status->name."</span>";
                    },'label'=>'Status','filter'=>false],
                    ['attribute'=>'customer_id','label'=>'Customer','format'=>'raw','value'=>function($model){
                        return $model->customer ? Html::a($model->customer->name,['customers/view','id'=>$model->customer_id]) : NULL;
                    }],
                    ['attribute'=>'amount_charged','format'=>'raw','value'=>function($model){
                        return '<span class="badge badge-danger"><b>'.number_format($model->amount_charged).'</b></span>';
                    },'label'=>'Amount'],

                    ['class' => 'yii\grid\ActionColumn','template'=>'{view}'],
                ],
            ]); ?>
        </div>

    </div>
</div>
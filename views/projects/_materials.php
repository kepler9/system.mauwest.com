<?php
	use yii\widgets\ActiveForm;
	use yii\helpers\Html;
?>
<?php $form = ActiveForm::begin()?>
<div id="material-items" class="row">
	<div class="col-md-6">
		<div class="form-group">
			<label>Item</label>
			<input class="form-control" name="item[]" data-validation="required">
		</div>
	</div>
	<div class="col-md-2">
		<div class="form-group">
			<label>Quantity</label>
			<input class="form-control" name="quantity[]" data-validation="number">
		</div>
	</div>
	<div class="col-md-2">
		<div class="form-group">
			<?=$this->render('//measure-units/_options');?>
		</div>
	</div>
	<div class="col-md-2">
		<div class="form-group">
			<label>Rate</label>
			<input class="form-control" name="rate[]" data-validation="number" data-validation-allowing="float">
		</div>
	</div>
</div>
<div class="form-group">
	<a id="add-materials-item" class="active" href="#"><i class="fa fa-plus-circle"></i> Add Item</a>
</div>
<hr class="hr-25">
<div class="form-group">
    <?= Html::a('Back', ['projects/create','step'=>2],['class' => 'btn btn-secondary btn-sm pull-left']) ?>
    <?= Html::submitButton('Submit', ['class' => 'btn btn-success btn-sm pull-right']) ?>
    <?= Html::a('Skip&nbsp;&nbsp;', ['create','step'=>4], ['class' => 'pull-right','style'=>'font-weight:bold;position:relative;top:3px;']) ?>
    <span class="clearfix"></span>
</div>
<?php ActiveForm::end();?>
<script type="text/javascript">
	$('#add-materials-item').click(function(event) {
		event.preventDefault();
		$('#material-items').append('<div class="col-md-6"><div class="form-group"><label>Item</label><input class="form-control" name="item[]" data-validation="required"></div></div><div class="col-md-2"><div class="form-group"><label>Quantity</label><input class="form-control" name="quantity[]" data-validation="number"></div></div><div class="col-md-2"><div class="form-group units-of-measure"></div></div><div class="col-md-2"><div class="form-group"><label>Rate</label><input class="form-control" name="rate[]" data-validation="number" data-validation-allowing="float"></div></div>');
		$.get('/measure-units/options', function(data) {
			$('.units-of-measure').last().html(data);
		});
	});
</script>
<?php
	use yii\helpers\Html;
	$this->title = $title;
	$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ibox">
    
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <ul class="list-inline">
				<li class="list-inline-item step <?=$step == 1 ? 'active-step' : ''; ?>"><span>1</span></li>
                <li class="list-inline-item step <?=$step == 2 ? 'active-step' : ''; ?>"><span>2</span></li>
                <li class="list-inline-item step <?=$step == 3 ? 'active-step' : ''; ?>"><span>3</span></li>
                <li class="list-inline-item step <?=$step == 4 ? 'active-step' : ''; ?>"><span>4</span></li>
			</ul>
        </div>
    </div>

    <div class="ibox-body">
    	<?= $this->render($form, [
	        'model' => $model,
            'lists' => $lists,
            'update' => 0
	    ]) ?>
    </div>

</div>


<?php
	use yii\helpers\Html;
	$this->title = 'Update Project: ' . $model->name;
	$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
	$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->project_id]];
	$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <!-- actions -->
        </div>
    </div>
    <div class="ibox-body">
    	<?= $this->render('_form', [
	        'model' => $model,
            'type' => $type,
	    ]) ?>
    </div>
</div>

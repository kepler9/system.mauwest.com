<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SystemSettings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="system-settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'settings')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'value')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-sm pull-right']) ?>
        <span class="clearfix"></span>
    </div>

    <?php ActiveForm::end(); ?>

</div>

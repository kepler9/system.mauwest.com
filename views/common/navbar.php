<?php
    use yii\helpers\Html;
    use app\models\MailQueue;
    $NotificationsCount = MailQueue::find()->where(['sent'=>0])->count();
?>
<header class="header">
<div class="page-brand">
    <a href="/">
        <span class="brand"><?=Yii::$app->params['company']?></span>
        <span class="brand-mini"><?=Yii::$app->params['abbr']?></span>
    </a>
</div>
<div class="flexbox flex-1">
    <ul class="nav navbar-toolbar">
        <li>
            <a class="nav-link sidebar-toggler js-sidebar-toggler" id="toggle_sidebar"  href="javascript:;">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
        </li>
    </ul>
    <ul class="nav navbar-toolbar">
        <li class="dropdown dropdown-notification">
            <a class="nav-link dropdown-toggle toolbar-icon" data-toggle="dropdown" href="javascript:;"><i class="ti-bell rel"></i> <span class="envelope-badge"><?=number_format($NotificationsCount);?></span></a>
            <div class="dropdown-menu dropdown-menu-right dropdown-menu-media">
                <div class="dropdown-arrow"></div>
                <div class="dropdown-header text-center">
                    <div>
                        <span class="font-18"><strong><?=number_format($NotificationsCount);?> New</strong> Notifications</span>
                    </div>
                    <a class="text-muted font-13" href="/mail-queue/index">View All</a>
                </div>
                    <div class="p-3">

                    </div>
                </div>
                    </li>
                    <li class="dropdown dropdown-user show">
                        <a class="nav-link dropdown-toggle link" data-toggle="dropdown" aria-expanded="true">
                            <span><?=ucfirst(Yii::$app->user->identity->username)?></span>
                            <?=Html::img('/uploads/avatar.png');?>
                        </a>
                        <div class="dropdown-menu dropdown-arrow dropdown-menu-right admin-dropdown-menu show" x-placement="top-end" style="position: absolute; transform: translate3d(-300px, -405px, 0px); top: 0px; left: 0px; will-change: transform;">
                            <div class="dropdown-arrow"></div>
                            <div class="dropdown-header">
                                <div>
                                    <h5 class="font-strong text-white">Admin</h5>
                                </div>
                            </div>
                            <div class="admin-menu-features">
                                <a class="admin-features-item" href="/admins/profile"><i class="ti-user"></i>
                                    <span>Profile</span>
                                </a>
                                <a data-method="post" class="admin-features-item" href="/site/logout"><i class="fa fa-sign-out"></i>
                                    <span>Logout</span>
                                </a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </header>

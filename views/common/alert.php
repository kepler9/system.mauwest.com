<?php if(Yii::$app->session->hasFlash('msg')): ?>
	<div class="alert alert-<?=Yii::$app->session->hasFlash('error_msg') ? 'danger' : 'success'; ?>">
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
	    <strong><?=Yii::$app->session->getFlash('msg');?></strong>
	</div>
<?php endif; ?>
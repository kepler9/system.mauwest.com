<?php 
	$units = \app\models\MeasureUnits::find()->all();
?>
<label>Unit of Measure</label>
<select name="unit[]" class="form-control">
	<option value="">Select</option>
	<?php if($units): foreach($units as $unit):?>
		<option value="<?=$unit->name?>"><?=$unit->name?></option>
	<?php endforeach;endif;?>
</select>
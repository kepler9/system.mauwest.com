<?php
	use yii\helpers\Html;
	$this->title = 'Create';
	$this->params['breadcrumbs'][] = ['label' => 'Notifications', 'url' => ['index']];
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
        </div>
    </div>
    <div class="ibox-body">
    	<?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>
</div>

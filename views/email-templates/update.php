<?php
	use yii\helpers\Html;
	$this->title = 'Update Notification: ' . $model->subject;
	$this->params['breadcrumbs'][] = ['label' => 'Notifications', 'url' => ['index']];
	$this->params['breadcrumbs'][] = ['label' => $model->subject, 'url' => ['view', 'id' => $model->template_id]];
	$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
        </div>
    </div>
    <div class="ibox-body">
    	<?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>
</div>

<?php
    use yii\helpers\Html;
    use yii\grid\GridView;
    $this->title = 'Notifications';
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <?= Html::a('Create a Notification', ['create'], ['class' => 'btn btn-success btn-sm']) ?>
        </div>
    </div>
    <div class="ibox-body">
        <div class="table-responsive">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                // 'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'type',
                    'subject:ntext',
                    'body:html',
                    ['class' => 'yii\grid\ActionColumn','template'=>'{update}'],
                ],
            ]); ?>
        </div>
    </div>
</div>


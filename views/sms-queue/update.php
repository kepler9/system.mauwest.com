<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SmsQueue */

$this->title = 'Update Sms Queue: ' . $model->queue_id;
$this->params['breadcrumbs'][] = ['label' => 'Sms Queues', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->queue_id, 'url' => ['view', 'id' => $model->queue_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sms-queue-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

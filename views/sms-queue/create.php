<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SmsQueue */

$this->title = 'Create Sms Queue';
$this->params['breadcrumbs'][] = ['label' => 'Sms Queues', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sms-queue-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

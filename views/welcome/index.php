<?php
    $this->title = 'Welcome '.ucfirst(Yii::$app->user->identity->username);
    $list = \app\models\CustomerLists::findOne(['type'=>'default']);
?>
<div class="ibox ibox-fullheight">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <!-- actions -->
        </div>
    </div>
    <div class="ibox-body">
        <div class="row">
            <div class="col-md-3">
                <div onclick="window.location='/projects/create'" class="ibox bg-danger text-white link">
                    <div class="ibox-body">
                        <h2 class="mb-1"><i class="fa fa-plus"></i> </h2>
                        <div class="text-white">Create Project</div><i class="la la-book widget-stat-icon"></i>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div onclick="window.location='/projects/index'" class="ibox bg-info text-white link">
                    <div class="ibox-body">
                        <h2 class="mb-1"><?=number_format(\app\models\Projects::find()->count());?></h2>
                        <div class="text-white">Projects</div><i class="la la-folder-open widget-stat-icon"></i>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div onclick="window.location = '/customers/index?list_id=<?=$list->list_id?>'" class="ibox bg-light-dark text-white link">
                    <div class="ibox-body">
                        <h2 class="mb-1"><?=number_format(\app\models\Customers::find()->where(['list_id'=>$list->list_id])->count());?></h2>
                        <div class="text-white">Customers</div><i class="la la-users widget-stat-icon"></i>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div onclick="window.location = '/casuals/index'" class="ibox bg-warning text-white link">
                    <div class="ibox-body">
                        <h2 class="mb-1"><?=number_format(\app\models\Casuals::find()->count());?></h2>
                        <div class="text-white">Casuals</div><i class="la la-users widget-stat-icon"></i>
                    </div>
                </div>
            </div>
        </div>
        <?php
            $total = \app\models\Projects::find()->count();
            $statuses = \app\models\ProjectStatus::find()->all();
        ?>
        <div class="row">
            <div class="col-md-6">
                <?=
                  \dosamigos\highcharts\HighCharts::widget([
                      'clientOptions' => [
                          'chart' => ['type' => 'pie'],
                          'title' => ['text' => 'Projects Status Report'],
                          'series' => [
                              ['name' => 'Total', 'data' => $this->context->model->projects_pie_report()],
                          ]
                      ]
                  ]);?>
              </div>
              <div class="col-md-6">
                  <div style="margin-top: 15px;" class="table-responsive">
                      <table class="table table-bordered">
                          <tbody>
                            <?php if($statuses): foreach($statuses as $status):?>
                              <?php $ratio = \app\models\Projects::find()->where(['status_id'=>$status->status_id])->count(); ?>
                              <tr>
                                  <td style="width:50%;background-color:#f5f5f5;"><?=$status->name;?></td>
                                  <td><a href="/projects/index?status_id=<?=$status->status_id?>"><b><?=$ratio?> > <?=number_format($ratio/$total*100)?>%</b></a></td>
                              </tr>
                            <?php endforeach;endif;?>
                          </tbody>
                      </table>
                  </div>
              </div>
        </div>
    </div>
</div>
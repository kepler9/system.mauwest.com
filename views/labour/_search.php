<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EstimatedLabourSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="estimated-labour-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'estimated_labour_id') ?>

    <?= $form->field($model, 'project_id') ?>

    <?= $form->field($model, 'quantity') ?>

    <?= $form->field($model, 'rate') ?>

    <?= $form->field($model, 'total') ?>

    <?php // echo $form->field($model, 'title') ?>

    <?php // echo $form->field($model, 'date_created') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

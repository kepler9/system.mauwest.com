<?php
	use yii\helpers\Html;
	$type = $model->type ? 'Actual' : 'Estimated';
	$this->title = 'Update '.$type.' Labour: ' . $model->title;
	$this->params['breadcrumbs'][] = ['label'=>'Projects','url'=>['/projects/index']];
	$this->params['breadcrumbs'][] = ['label'=>$model->project->description,'url'=>['/projects/view','id'=>$model->project_id]];
	$this->params['breadcrumbs'][] = ['label' => $type.' Labour', 'url' => ['index','type'=>$model->type]];
	$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->labour_id]];
	$this->params['breadcrumbs'][] = 'Update';
?>

<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
        </div>
    </div>
    <div class="ibox-body">
    	<?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>
</div>


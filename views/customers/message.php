<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
$this->title = 'Send SMS';
$this->params['breadcrumbs'][] = ['label' => 'Customers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $customer->name, 'url' => ['view', 'id' => $customer->customer_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <!-- actions -->
        </div>
    </div>
    <div class="ibox-body">
    	<?php $form = ActiveForm::begin(); ?>
    		<?=$form->field($queue,'message')->textArea();?>
    		<div class="form-group pull-right">
		        <?= Html::submitButton('Send', ['class' => 'btn btn-sm btn-success']) ?>
		    </div>
		    <span class="clearfix"></span>
    	<?php ActiveForm::end();?>
    </div>
</div>

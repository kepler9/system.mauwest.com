<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
?>

<div class="customers-form">
    <div class="boxed">
        <div class="ibox-head flex-row-reverse">
            <div class="ibox-title"></div>
            <ul class="nav nav-pills nav-pills-rounded nav-pills-air">
                <li class="nav-item">
                    <a class="nav-link active" href="#tab-2-1" data-toggle="tab">Use a Form</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#tab-2-2" data-toggle="tab">Upload Excel</a>
                </li>
            </ul>
        </div>
        <div class="ibox-body">
            <div class="tab-content">
                <div class="tab-pane fade show active" id="tab-2-1">
                   <?=$this->render('__form',['lists'=>$lists,'model'=>$model]);?>
                </div>
                <div class="tab-pane fade" id="tab-2-2">
                    <?php $form = ActiveForm::begin(['id'=>'excel_upload_form']); ?>
                    
                    <label>Customer List | <a href="/customer-lists/create?redirect=<?=Yii::$app->request->url?>"><i class="fa fa-plus"></i> Add</a></label>
                    <?= $form->field($model,'list_id')->dropDownList(ArrayHelper::map($lists,'list_id','name'),['prompt'=>'Select'])->label(false);?>

                    <label><?='Upload Excel File | '.Html::a('Download Template',['template'])?></label>
                    <?= $form->field($ExcelFileUpload, 'UploadFile')->fileInput()->label(false) ?>

                    <input type="hidden" name="form" value="upload">

                    <div class="form-group">
                        <?= Html::submitButton('Submit', ['class' => 'btn btn-success btn-sm pull-right']) ?>
                        <span class="clearfix"></span>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

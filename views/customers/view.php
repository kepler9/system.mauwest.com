<?php
    use yii\helpers\Html;
    use yii\widgets\DetailView;
    $this->title = $model->name;
    $this->params['breadcrumbs'][] = ['label' => 'Customers', 'url' => ['index']];
    if($project):
        $this->params['breadcrumbs'][] = ['label'=>'Projects','url'=>['/projects/index']];
        $this->params['breadcrumbs'][] = ['label'=>$project->name,'url'=>['/projects/view','id'=>$project->project_id]];
    endif;
    $this->params['breadcrumbs'][] = $this->title;
    \yii\web\YiiAsset::register($this);
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <?= Html::a('Update', ['update', 'id' => $model->customer_id], ['class' => 'btn btn-primary btn-sm']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->customer_id], [
                'class' => 'btn btn-danger btn-sm',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </div>
    </div>
    <div class="ibox-body">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'name',
                ['attribute'=>'phone','format'=>'raw','value'=>function($model){
                    return Html::a($model->phone,['message','customer_id'=>$model->customer_id,'redirect'=>Yii::$app->request->url]);
                }],
                'email:email',
                ['attribute'=>'status','format'=>'raw','value'=>function($model){
                    return $model->status ? '<span class="badge badge-success">active</span>' : '<span class="badge badge-danger">inactive</span>';
                }],
                ['attribute'=>'list.name','label'=>'Customer List'],
                'company',
                'date_created:date',
            ],
        ]) ?>
    </div>
</div>

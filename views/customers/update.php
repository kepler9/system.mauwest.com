<?php
use yii\helpers\Html;
$this->title = 'Update Customers: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Customers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->customer_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <!-- actions -->
        </div>
    </div>
    <div class="ibox-body">
    	<?= $this->render('__form', [
	        'model' => $model,
            'lists' => $lists,
	    ]) ?>
    </div>
</div>

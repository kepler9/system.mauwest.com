<?php
use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Customers';
$this->params['breadcrumbs'][] = ['label'=>$this->title,'url'=>['index']];
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <?=Html::a('Create','create',['class'=>'btn btn-primary btn-sm']);?>
        </div>
    </div>
    <div class="ibox-body">
        <div class="table-responsive">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                // 'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'name',
                    ['attribute'=>'phone','format'=>'raw','value'=>function($model){
                        return Html::a($model->phone,['message','customer_id'=>$model->customer_id,'redirect'=>Yii::$app->request->url]);
                    }],
                    'email:email',
                    ['attribute'=>'status','format'=>'raw','value'=>function($model){
                        return $model->status ? '<span class="badge badge-success">active</span>' : '<span class="badge badge-danger">inactive</span>';
                    }],
                    ['attribute'=>'list.name','label'=>'List'],
                    'company',
                    'date_created:date',

                    ['class' => 'yii\grid\ActionColumn','template'=>'{view}'],
                ],
            ]); ?>
        </div>
    </div>
</div>

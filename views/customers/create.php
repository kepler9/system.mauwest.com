<?php
use yii\helpers\Html;
$this->title = 'Create Customers';
$this->params['breadcrumbs'][] = ['label' => 'Customers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <!-- actions -->
        </div>
    </div>
    <div class="ibox-body">
    	<div class="customers-create">

		    <?= $this->render('_form', [
		        'model' => $model,
		        'lists' => $lists,
		        'ExcelFileUpload' => $ExcelFileUpload,
		    ]) ?>

		</div>
    </div>
</div>

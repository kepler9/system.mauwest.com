<?php
    use app\assets\AppAsset;
    use app\widgets\Alert;
    use yii\helpers\Html;
    use yii\bootstrap\Nav;
    use yii\bootstrap\NavBar;
    use yii\widgets\Breadcrumbs;
    AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="icon" href="/favicon.png">
    <?php $this->head() ?>
    <link rel="stylesheet" href="/assets/css/styles.css?v=<?=time()?>">
</head>
<body class="fixed-navbar">
    <?php $this->beginBody() ?>
    <div class="page-wrapper">
        <?=$this->render('//common/navbar');?>
        <?=$this->render('//common/sidebar');?>
        <div class="content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-lg-12">
                        <?= Breadcrumbs::widget([
                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        ]) ?>
                        <?=$this->render('//common/alert');?>
                        <?= $content ?>
                    </div>
                </div>
            </div>
            <?=$this->render('//common/footer');?>
        </div>
    </div>
    <div class="sidenav-backdrop backdrop"></div>
    <?php $this->endBody() ?>
    <script type="text/javascript">
        $.validate({
            lang: 'en'
        });
        $(function() {
            $('.datatable').DataTable({
                pageLength: 5,
                fixedHeader: true,
                responsive: true,
                "sDom": 'rtip',
                columnDefs: [{
                    targets: 'no-sort',
                    orderable: false
                }]
            });
            $('.filters input').attr('placeholder', 'Search here..');
            $('[name="LabourSearch[date_created]"]').attr('type', 'date');
            $('[name="MaterialsSearch[date_created]"]').attr('type', 'date');
            $('[name="MiscCostsSearch[date_created]"]').attr('type', 'date');
        });
    </script>
</body>
</html>
<?php $this->endPage() ?>
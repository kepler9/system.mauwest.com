<?php
use yii\helpers\Html;
$this->title = 'Create Customer List';
$this->params['breadcrumbs'][] = ['label' => 'Customer Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <!-- actions -->
        </div>
    </div>
    <div class="ibox-body">
    	<div class="customer-lists-create">

		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>

		</div>
    </div>
</div>

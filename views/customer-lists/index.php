<?php
use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Customer Lists';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <?= Html::a('Create Customer List', ['create'], ['class' => 'btn btn-success btn-sm']) ?>
        </div>
    </div>
    <div class="ibox-body">
        <div class="customer-lists-index">

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                // 'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'name',
                    'type',
                    ['attribute'=>'list_id','format'=>'raw','label'=>'No. of Customers','value'=>function($model){
                        return Html::a(count($model->customers),['customers/index','list_id'=>$model->list_id]);
                    }],
                    'date_created',

                    ['class' => 'yii\grid\ActionColumn','template'=>'{view}'],
                ],
            ]); ?>

        </div>
    </div>
</div>

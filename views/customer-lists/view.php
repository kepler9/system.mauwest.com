<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerLists */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Customer Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <?= Html::a('Update', ['update', 'id' => $model->list_id], ['class' => 'btn btn-primary btn-sm']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->list_id], [
                'class' => 'btn btn-danger btn-sm',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </div>
    </div>
    <div class="ibox-body">
        <div class="customer-lists-view">

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'name',
                    'type',
                    ['attribute'=>'list_id','format'=>'raw','label'=>'No. of Customers','value'=>function($model){
                        return Html::a(count($model->customers),['customers/index','list_id'=>$model->list_id]);
                    }],
                    'date_created',
                ],
            ]) ?>

        </div>
    </div>
</div>

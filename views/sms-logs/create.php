<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SmsLogs */

$this->title = 'Create Sms Logs';
$this->params['breadcrumbs'][] = ['label' => 'Sms Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sms-logs-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

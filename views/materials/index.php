<?php
    use yii\helpers\Html;
    use yii\grid\GridView;
    $type = Yii::$app->request->get('type') ? 'Actual' : 'Estimated';
    $this->title = $type.' Materials';
    if(Yii::$app->request->get('type')){
        $total = $this->context->model->actual_materials_cost($this->context->project->project_id);
    }else{
        $total = $this->context->model->estimated_materials_cost($this->context->project->project_id);
    }
    $this->params['breadcrumbs'][] = ['label'=>'Projects','url'=>['/projects/index']];
    $this->params['breadcrumbs'][] = ['label'=>$this->context->project->name,'url'=>['/projects/view','id'=>$this->context->project->project_id]];
    $this->params['breadcrumbs'][] = ['label'=>$this->title,'url'=>['/materials/index','type'=>Yii::$app->request->get('type')]];
?>

<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <?= Html::a('Add '.$type.' Materials', ['create','type'=>Yii::$app->request->get('type')], ['class' => 'btn btn-success btn-sm']) ?>
        </div>
    </div>
    <div class="ibox-body">
        <div class="btn-group btn-rounded pull-right">
            <div class="btn btn-outline-secondary btn-sm"><?=Html::a('Total KSh <b>'.number_format($total).'</b>',['materials/index','type'=>Yii::$app->request->get('type')]);?></div>
            <div class="btn btn-outline-secondary btn-sm"><?=Html::a('<i class="fa fa-download"></i> Download',['materials/index','type'=>Yii::$app->request->get('type'),'download'=>1]);?></div>
        </div>
        <span class="clearfix"></span>

        <div style="margin-top:20px" class="table-responsive">

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'item:ntext',
                    ['attribute'=>'quantity','filter'=>false,'value'=>function($model){
                        return $model->quantity ? number_format($model->quantity).' '.$model->unit : NULL;
                    }],
                    ['attribute'=>'rate','filter'=>false],
                    ['attribute'=>'total','format'=>'decimal','filter'=>false],
                    ['attribute'=>'date_created','format'=>'date','label'=>'Date'],

                    ['class' => 'yii\grid\ActionColumn','template'=>'{view}'],
                ],
            ]); ?>

        </div>
    </div>
</div>

<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use yii\helpers\ArrayHelper;
    $type = Yii::$app->request->get('type') ?? $model->type;
    if($model->date_created){
        $model->date_created = date('Y-m-d',strtotime($model->date_created));
    }
?>
<div class="estimated-materials-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'item')->textarea(['rows' => 1,'placeholder'=>'Enter item name']) ?>
    <?= $form->field($model, 'unit')->dropDownList(ArrayHelper::map(\app\models\MeasureUnits::find()->all(),'name','name'),['prompt'=>'Select','data-validation'=>'required'])->label('Unit of Measure') ?>
    <?= $form->field($model, 'quantity')->textInput(['placeholder'=>'Enter item quantity','data-validation'=>'required']) ?>    
    <?= $form->field($model, 'rate')->textInput(['placeholder'=>'Enter rate of cost','data-validation'=>'required']); ?>
    <?php if($type == 1):?>
        <?= $form->field($model, 'date_created')->textInput(['type'=>'date','data-validation'=>'required'])->label('Date'); ?>
    <?php endif;?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-sm pull-right']) ?>
        <span class="clearfix"></span>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Document Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <?= Html::a('Update', ['update', 'id' => $model->document_type_id], ['class' => 'btn btn-primary btn-sm']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->document_type_id], [
                'class' => 'btn btn-danger btn-sm',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </div>
    </div>
    <div class="ibox-body">
        <div class="document-types-view">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'name',
                    ['attribute'=>'emailed','format'=>'raw','value'=>function($model){
                        return $model->emailed ? 'Yes' : 'No';
                    },'label'=>'Can be emailed to client'],
                ],
            ]) ?>
        </div>
    </div>
</div>
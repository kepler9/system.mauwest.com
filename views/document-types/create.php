<?php
use yii\helpers\Html;
$this->title = 'Add Document Type';
$this->params['breadcrumbs'][] = ['label' => 'Document Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <!-- actions -->
        </div>
    </div>
    <div class="ibox-body">
    	<div class="document-types-create">

		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>

		</div>
    </div>
</div>
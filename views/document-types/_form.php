<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="document-types-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'emailed')->dropDownList([0=>'No',1=>'Yes'])->label('Can be emailed to client') ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-sm pull-right']) ?>
        <span class="clearfix"></span>
    </div>

    <?php ActiveForm::end(); ?>

</div>

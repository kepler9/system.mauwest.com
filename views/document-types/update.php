<?php
use yii\helpers\Html;
$this->title = 'Update Document Type: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Document Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->document_type_id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <!-- actions -->
        </div>
    </div>
    <div class="ibox-body">
    	<div class="document-types-update">

		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>

		</div>
    </div>
</div>
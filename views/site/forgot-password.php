<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = 'Login';
?>
<style>
    body {
        background-repeat: no-repeat;
        background-size: cover;
        background-image: url('/assets/img/blog/17.jpg');
    }

    .cover {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        background-color: rgba(117, 54, 230, .1);
    }

    .login-content {
        max-width: 400px;
        margin: 100px auto 50px;
    }

    .auth-head-icon {
        position: relative;
        height: 60px;
        width: 60px;
        display: inline-flex;
        align-items: center;
        justify-content: center;
        font-size: 30px;
        background-color: #fff;
        color: #5c6bc0;
        box-shadow: 0 5px 20px #d6dee4;
        border-radius: 50%;
        transform: translateY(-50%);
        z-index: 2;
    }
</style>
<div class="cover"></div>
<div class="ibox login-content">
    <div class="text-center">
        <span class="auth-head-icon"><i class="la la-key"></i></span>
    </div>
    <?php $form = ActiveForm::begin(['id' => 'forgot-form','options' => ['class' => "ibox-body pt-0"],]); ?>
    <h4 class="font-strong text-center mb-4">FORGOT PASSWORD</h4>
    <p class="mb-4">Enter your username below and we'll send you password reset instructions.</p>
    <div class="mb-4">
        <?= $form->field($model, 'username')->textInput(['autofocus' => true,'class'=>'form-control form-control-line','placeholder'=>'Username'])->label(false) ?>
    </div>
    <div class="mb-4">
        <?=Html::a('Back to login?',['/'],['class'=>'text-primary pull-right']);?>
        <span class="clearfix"></span>
    </div>
    <div class="text-center mb-4">
        <button class="btn btn-primary btn-rounded btn-block">SUBMIT</button>
    </div>
    <?php ActiveForm::end(); ?>
</div>

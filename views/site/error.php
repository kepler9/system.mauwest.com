<?php

use yii\helpers\Html;

$this->title = $name;
?>
<style>
    body {
        background-color: #fff;
        background-repeat: no-repeat;
        background-image: url(/assets/img/icons/server-error-2.svg);
        background-position: 80% 10px;
    }

    .error-content {
        max-width: 620px;
        margin: 200px auto 0;
    }

    .error-icon {
        height: 160px;
        width: 160px;
        background-image: url(/assets/img/icons/server-error.svg);
        background-size: cover;
        background-repeat: no-repeat;
        margin-right: 80px;
    }

    .error-code {
        font-size: 120px;
        color: #5c6bc0;
    }
</style>
<div class="error-content flexbox">
    <span class="error-icon"></span>
    <div class="flex-1">
        <h3 class="font-strong"><?= Html::encode($this->title) ?></h3>
        <div class="alert alert-danger">
            <p class="mb-0"><?= nl2br(Html::encode($message)) ?></p>
        </div>
        <p>
            The above error occurred while the Web server was processing your request.
        </p>
        <p>
            Please contact us if you think this is a server error. Thank you.
        </p>
        <div>
            <a class="text-primary" href="/">Go Homepage</a>
        </div>
    </div>
</div>

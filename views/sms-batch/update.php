<?php
use yii\helpers\Html;
$this->title = 'Update Message';
$this->params['breadcrumbs'][] = ['label' => 'Bulk SMS Schedules', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'SMS', 'url' => ['view', 'id' => $model->batch_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <!-- actions -->
        </div>
    </div>
    <div class="ibox-body">
    	<?= $this->render('_form', [
	        'model' => $model,
	        'lists' => $lists,
	    ]) ?>
    </div>
</div>
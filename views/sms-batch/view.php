<?php
    use yii\helpers\Html;
    use yii\widgets\DetailView;
    use yii\grid\GridView;
    $this->title = 'SMS';
    $this->params['breadcrumbs'][] = ['label' => 'Bulk SMS Schedules', 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
    \yii\web\YiiAsset::register($this);
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <?= Html::a('Update', ['update', 'id' => $model->batch_id], ['class' => 'btn btn-primary btn-sm']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->batch_id], [
                'class' => 'btn btn-danger btn-sm',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </div>
    </div>
    
    <div class="ibox-body">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'message:ntext',
                ['attribute'=>'send_date','label'=>'When to Send'],
                ['attribute'=>'list.name','label'=>'Customer List'],
            ],
        ]) ?>
    </div>

    <div class="ibox-body">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" href="#sms-queue" data-toggle="tab">SMS Queue </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#sms-reports" data-toggle="tab">SMS Reports</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade show active" id="sms-queue">
                <?= GridView::widget([
                    'dataProvider' => $SmsQueueDataProvider,
                    'columns' => [
                        ['class'=>'yii\grid\SerialColumn'],
                        ['attribute'=>'customer.name','label'=>'Customer'],
                        ['attribute'=>'phone'],
                        ['attribute'=>'message'],
                        ['attribute'=>'status','format'=>'raw','value'=>function($model){
                            return '<span class="badge badge-info">'.$model->status.'</span>';
                        }],
                        ['class'=>'yii\grid\ActionColumn','template'=>'{delete}','buttons'=>[
                            'delete' => function($url,$model,$id){
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>',['/sms-queue/delete','id'=>$id,'redirect'=>Yii::$app->request->url],['data'=>['confirm'=>'Are you sure to delete this message?','method'=>'POST']]);
                            }
                        ]],
                    ],
                ]) ?>
            </div>
            <div class="tab-pane fade" id="sms-reports">
                <?= GridView::widget([
                    'dataProvider' => $SmsLogsDataProvider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        ['attribute'=>'customer.name','label'=>'Customer'],
                        'message:ntext',
                        'phone',
                        ['attribute'=>'status','format'=>'raw','value'=>function($model){
                            return '<span class="badge badge-info">'.$model->status.'</span>';
                        }],
                        'cost',
                        'date_sent:date',

                        ['class' => 'yii\grid\ActionColumn','template'=>NULL],
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
<?php
use yii\helpers\Html;
$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => 'Bulk SMS', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <!-- actions -->
        </div>
    </div>
    <div class="ibox-body">
    	<?= $this->render('_form', [
	        'model' => $model,
	        'lists' => $lists,
	    ]) ?>
    </div>
</div>
<?php
use yii\helpers\Html;
$this->title = 'Update Casuals: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Casuals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->casual_id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <!-- actions -->
        </div>
    </div>
    <div class="ibox-body">
    	<div class="casuals-update">

		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>

		</div>
    </div>
</div>

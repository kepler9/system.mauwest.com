<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="casuals-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true,'data-validation'=>'number']) ?>

    <?= $form->field($model, 'daily_rate')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-sm pull-right']) ?>
    </div>
    <span class="clearfix"></span>

    <?php ActiveForm::end(); ?>

</div>

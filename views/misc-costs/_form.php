<?php
	use yii\helpers\Html;
	use yii\widgets\ActiveForm;
?>
<div class="misc-costs-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'item')->textarea(['rows' => 1]) ?>
    <?= $form->field($model, 'cost')->textInput() ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-sm pull-right']) ?>
        <span class="clearfix"></span>
    </div>
    <?php ActiveForm::end(); ?>
</div>
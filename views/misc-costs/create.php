<?php
	use yii\helpers\Html;
	$this->title = 'Add Miscellaneous Costs';
	$this->params['breadcrumbs'][] = ['label'=>'Projects','url'=>['/projects/index']];
	$this->params['breadcrumbs'][] = ['label'=>$this->context->project->name,'url'=>['/projects/view','id'=>$this->context->project->project_id]];
	$this->params['breadcrumbs'][] = ['label' => 'Miscellaneous Costs', 'url' => ['index','project_id'=>$this->context->project->project_id]];
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <!-- actions -->
        </div>
    </div>
    <div class="ibox-body">
    	<?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>
</div>
<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
$this->title = $model->item;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['projects/index']];
$this->params['breadcrumbs'][] = ['label' => $model->project->name, 'url' => ['projects/view','id'=>$model->project_id]];
$this->params['breadcrumbs'][] = ['label' => 'Miscellaneous Costs', 'url' => ['index','project_id'=>$model->project_id]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <?= Html::a('Update', ['update', 'id' => $model->misc_cost_id], ['class' => 'btn btn-primary btn-sm']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->misc_cost_id], [
                'class' => 'btn btn-danger btn-sm',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </div>
    </div>
    <div class="ibox-body">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'item:ntext',
                'cost',
                ['attribute'=>'project.description','label'=>'Project'],
                'date_created',
            ],
        ]) ?>
    </div>
</div>
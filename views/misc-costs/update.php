<?php
	use yii\helpers\Html;
	$this->title = 'Update Miscellaneous Costs: ' . $model->item;
	$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['projects/index']];
	$this->params['breadcrumbs'][] = ['label' => $model->project->name, 'url' => ['projects/view','id'=>$model->project_id]];
	$this->params['breadcrumbs'][] = ['label' => 'Miscellaneous Costs', 'url' => ['index','project_id'=>$model->project_id]];
	$this->params['breadcrumbs'][] = ['label' => $model->item, 'url' => ['view', 'id' => $model->misc_cost_id]];
	$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <!-- actions -->
        </div>
    </div>
    <div class="ibox-body">
    	<?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>
</div>
<?php
use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Miscellaneous Costs';
$this->params['breadcrumbs'][] = ['label'=>'Projects','url'=>['/projects/index']];
$this->params['breadcrumbs'][] = ['label'=>$this->context->project->name,'url'=>['/projects/view','id'=>$this->context->project->project_id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title"><?=$this->title?></div>
        <div class="ibox-tools">
            <?= Html::a('Add Miscellaneous Costs', ['create','project_id'=>$this->context->project->project_id], ['class' => 'btn btn-success btn-sm']) ?>
        </div>
    </div>
    <div class="ibox-body">
        <div class="btn-group btn-rounded pull-right">
            <div class="btn btn-outline-secondary btn-sm"><?=Html::a('Total KSh <b>'.number_format($this->context->model->misc_costs($this->context->project->project_id)).'</b>',['misc-costs/index','project_id'=>$this->context->project->project_id]);?></div>
            <div class="btn btn-outline-secondary btn-sm"><?=Html::a('<i class="fa fa-download"></i> Download',['misc-costs/index','download'=>1,'project_id'=>$this->context->project->project_id]);?></div>
        </div>
        <span class="clearfix"></span>
        <div style="margin-top:20px">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'item:ntext',
                    ['attribute'=>'cost','format'=>'decimal','filter'=>false],
                    ['attribute'=>'date_created','label'=>'Date','format'=>'date'],

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
</div>
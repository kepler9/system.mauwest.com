<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        "assets/vendors/font-awesome/css/font-awesome.min.css",
        "assets/vendors/line-awesome/css/line-awesome.min.css",
        "assets/vendors/themify-icons/css/themify-icons.css",
        "assets/vendors/animate.css/animate.min.css",
        "assets/vendors/toastr/toastr.min.css",
        "assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css",
        "assets/css/glyphicons.css",
        "assets/vendors/dataTables/datatables.min.css",
        "assets/css/main.min.css",
    ];

    public $js = [
        "assets/vendors/popper.js/dist/umd/popper.min.js",
        "assets/vendors/bootstrap/dist/js/bootstrap.min.js",
        "assets/vendors/metisMenu/dist/metisMenu.min.js",
        "assets/vendors/jquery-slimscroll/jquery.slimscroll.min.js",
        "assets/vendors/jquery-idletimer/dist/idle-timer.min.js",
        "assets/vendors/toastr/toastr.min.js",
        "assets/vendors/jquery-validation/dist/jquery.validate.min.js",
        "assets/vendors/bootstrap-select/dist/js/bootstrap-select.min.js",
        "assets/vendors/dataTables/datatables.min.js",
        "assets/js/app.min.js",
        'js/jquery.form-validator.min.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}

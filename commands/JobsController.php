<?php
namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\Models;
use yii;

class JobsController extends Controller
{
    public $model;

    public function beforeAction($action){
        $this->model = new Models();
        return parent::beforeAction($action);
    }

    public function actionMinute() {
        $this->model->send_emails();
        $this->model->sms_batch();
        $this->model->send_sms();
        return ExitCode::OK;
    }

    public function actionMail(){
        Yii::$app->mailer->compose()
            ->setFrom('app@mauwest.com')
            ->setTo('kinoti.raphs@gmail.com')
            ->setSubject('Testing')
            ->setHtmlBody('<b>Hello there!</b>')
            ->send();
    }

    public function actionDaily() {
        $this->model->project_status();
        $this->model->progress_tracking();
        $this->model->send_emails();
        return ExitCode::OK;
    }

    public function actionWeekly(){
        $this->model->proposal_follow_up();
        $this->model->send_emails();
        return ExitCode::OK;
    }
}
